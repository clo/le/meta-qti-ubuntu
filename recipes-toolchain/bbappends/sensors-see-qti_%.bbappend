do_install_append (){
    #remove sscrpcd's requires data.mount service for ubuntu1.0
    sed -i '/Requires=data.mount/d' ${D}${systemd_unitdir}/system/sscrpcd.service
    sed -i '/After=data.mount/d' ${D}${systemd_unitdir}/system/sscrpcd.service
}
