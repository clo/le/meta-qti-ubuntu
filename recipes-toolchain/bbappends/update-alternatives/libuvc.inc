# use update-alternatives mechanism
inherit update-alternatives-ubuntu

# set update-alternatives priority for libuvc package
ALTERNATIVE_PRIORITY_libuvc = "100"

# set update-alternatives register name for libuvc package
ALTERNATIVE_libuvc = "libuvc.h libuvc_config.h libuvc.so libuvc.pc"

# set update-alternatives symbolic link path and real target path for libuvc package
ALTERNATIVE_LINK_NAME[libuvc.h] = "/usr/include/libuvc/libuvc.h"

ALTERNATIVE_LINK_NAME[libuvc_config.h] = "/usr/include/libuvc/libuvc_config.h"

ALTERNATIVE_LINK_NAME[libuvc.so] = "/usr/lib/aarch64-linux-gnu/libuvc.so"

ALTERNATIVE_LINK_NAME[libuvc.pc] = "/usr/lib/aarch64-linux-gnu/pkgconfig/libuvc.pc"
ALTERNATIVE_TARGET_libuvc[libuvc.pc] = "/usr/lib/pkgconfig/libuvc.pc"

