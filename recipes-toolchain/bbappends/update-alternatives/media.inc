# use update-alternatives mechanism
inherit update-alternatives-ubuntu

# set update-alternatives priority for media package
ALTERNATIVE_PRIORITY_media = "100"

# set update-alternatives register name for media package
ALTERNATIVE_media = "OMX_Audio.h OMX_Component.h OMX_ContentPipe.h OMX_Core.h OMX_IVCommon.h OMX_Image.h OMX_Index.h OMX_Other.h OMX_Types.h OMX_Video.h"

# set update-alternatives symbolic link path and real target path for media package
ALTERNATIVE_LINK_NAME[OMX_Audio.h] = "/usr/include/OMX_Audio.h"

ALTERNATIVE_LINK_NAME[OMX_Component.h] = "/usr/include/OMX_Component.h"

ALTERNATIVE_LINK_NAME[OMX_ContentPipe.h] = "/usr/include/OMX_ContentPipe.h"

ALTERNATIVE_LINK_NAME[OMX_Core.h] = "/usr/include/OMX_Core.h"

ALTERNATIVE_LINK_NAME[OMX_IVCommon.h] = "/usr/include/OMX_IVCommon.h"

ALTERNATIVE_LINK_NAME[OMX_Image.h] = "/usr/include/OMX_Image.h"

ALTERNATIVE_LINK_NAME[OMX_Index.h] = "/usr/include/OMX_Index.h"

ALTERNATIVE_LINK_NAME[OMX_Other.h] = "/usr/include/OMX_Other.h"

ALTERNATIVE_LINK_NAME[OMX_Types.h] = "/usr/include/OMX_Types.h"

ALTERNATIVE_LINK_NAME[OMX_Video.h] = "/usr/include/OMX_Video.h"

