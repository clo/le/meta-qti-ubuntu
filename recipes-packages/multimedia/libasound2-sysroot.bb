LICENSE = "LGPLv2.1+"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/a/alsa-lib/libasound2_1.1.3-5_arm64.deb'
DEB_NAME = 'libasound2_1.1.3-5_arm64.deb'

SRC_URI[md5sum] = "b1ee6f71f18276eb07401e6bdccd5727"
