
LICENSE = "BSD-3-clause"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/universe/o/opencv/libopencv-imgcodecs-dev_3.2.0+dfsg-4build2_arm64.deb'
DEB_NAME = 'libopencv-imgcodecs-dev_3.2.0+dfsg-4build2_arm64.deb'

SRC_URI[md5sum] = "2dfd33a244d95fd3a13b0eda924cd86b"
