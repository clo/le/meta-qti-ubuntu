LICENSE = "LGPLv2.1+"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/a/alsa-lib/libasound2-dev_1.1.3-5_arm64.deb'
DEB_NAME = 'libasound2-dev_1.1.3-5_arm64.deb'

SRC_URI[md5sum] = "52ff3e40ae8256be5eccdf38ced2bff1"
