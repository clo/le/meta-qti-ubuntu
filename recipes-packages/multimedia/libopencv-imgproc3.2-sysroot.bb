
LICENSE = "BSD-3-clause"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/universe/o/opencv/libopencv-imgproc3.2_3.2.0+dfsg-4build2_arm64.deb'
DEB_NAME = 'libopencv-imgproc3.2_3.2.0+dfsg-4build2_arm64.deb'

SRC_URI[md5sum] = "fbdfe59b901f51720854aa7cad9583e7"
