inherit native deb-dl

LICENSE = "LGPLv2.1+ & LGPLv2+"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/g/gstreamer1.0/libgstreamer1.0-dev_1.14.5-0ubuntu1~18.04.2_arm64.deb'
DEB_NAME = 'libgstreamer1.0-dev_1.14.5-0ubuntu1~18.04.2_arm64.deb'

SRC_URI[md5sum] = "33fca1a79a5de0ac565e26d37fa7bbd6"
