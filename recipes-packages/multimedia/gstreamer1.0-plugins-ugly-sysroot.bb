inherit native deb-dl

LICENSE = "GPLv2+ & LGPLv2+ & LGPLv2.1+"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/universe/g/gst-plugins-ugly1.0/gstreamer1.0-plugins-ugly_1.14.5-0ubuntu1~18.04.1_arm64.deb'
DEB_NAME = 'gstreamer1.0-plugins-ugly_1.14.5-0ubuntu1~18.04.1_arm64.deb'

SRC_URI[md5sum] = "0a188703162cd916b348824c25caafa8"
