inherit native deb-dl

LICENSE = "LGPLv2.1+ & LGPLv2+"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/g/gstreamer1.0/libgstreamer1.0-0_1.14.5-0ubuntu1~18.04.2_arm64.deb'
DEB_NAME = 'libgstreamer1.0-0_1.14.5-0ubuntu1~18.04.2_arm64.deb'

SRC_URI[md5sum] = "7bc7f3170470d51de8ece4b3633064d0"
