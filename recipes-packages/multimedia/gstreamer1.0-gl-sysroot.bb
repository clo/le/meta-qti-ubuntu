inherit native deb-dl

LICENSE = "GPLv2+ & LGPLv2+"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/g/gst-plugins-base1.0/gstreamer1.0-gl_1.14.5-0ubuntu1~18.04.3_arm64.deb'
DEB_NAME = 'gstreamer1.0-gl_1.14.5-0ubuntu1~18.04.3_arm64.deb'

SRC_URI[md5sum] = "efd8233e32f33eb70817141ca741f393"
