inherit native deb-dl

LICENSE = "GPLv2+ & LGPLv2+ & LGPLv2.1+"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/universe/g/gst-plugins-bad1.0/libgstreamer-plugins-bad1.0-0_1.14.5-0ubuntu1~18.04.1_arm64.deb'
DEB_NAME = 'libgstreamer-plugins-bad1.0-0_1.14.5-0ubuntu1~18.04.1_arm64.deb'

SRC_URI[md5sum] = "243ff44ffd83015f7f35bca9c75cd25e"
