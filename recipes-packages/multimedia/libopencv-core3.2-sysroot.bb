
LICENSE = "BSD-3-clause"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/universe/o/opencv/libopencv-core3.2_3.2.0+dfsg-4build2_arm64.deb'
DEB_NAME = 'libopencv-core3.2_3.2.0+dfsg-4build2_arm64.deb'

SRC_URI[md5sum] = "87752da48874e297d207fee3a8145f7c"
