
LICENSE = "BSD-3-clause"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/universe/o/opencv/libopencv-imgcodecs3.2_3.2.0+dfsg-4build2_arm64.deb'
DEB_NAME = 'libopencv-imgcodecs3.2_3.2.0+dfsg-4build2_arm64.deb'

SRC_URI[md5sum] = "28b370e63e2344f4a700f73993dccbcc"
