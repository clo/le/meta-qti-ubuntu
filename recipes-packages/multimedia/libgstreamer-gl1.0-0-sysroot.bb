inherit native deb-dl

LICENSE = "GPLv2+ & LGPLv2+"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/g/gst-plugins-base1.0/libgstreamer-gl1.0-0_1.14.5-0ubuntu1~18.04.3_arm64.deb'
DEB_NAME = 'libgstreamer-gl1.0-0_1.14.5-0ubuntu1~18.04.3_arm64.deb'

SRC_URI[md5sum] = "6dbce2d694922e44edcf124108827ac1"
