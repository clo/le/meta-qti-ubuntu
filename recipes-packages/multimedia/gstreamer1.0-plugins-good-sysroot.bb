inherit native deb-dl

LICENSE = "LGPLv2.1+ & LGPLv2+"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/g/gst-plugins-good1.0/gstreamer1.0-plugins-good_1.14.5-0ubuntu1~18.04.3_arm64.deb'
DEB_NAME = 'gstreamer1.0-plugins-good_1.14.5-0ubuntu1~18.04.3_arm64.deb'

SRC_URI[md5sum] = "c6e4de85aa3ba74d5a788002b91cb154"
