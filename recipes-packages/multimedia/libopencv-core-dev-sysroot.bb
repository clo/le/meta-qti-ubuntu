
LICENSE = "BSD-3-clause"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/universe/o/opencv/libopencv-core-dev_3.2.0+dfsg-4build2_arm64.deb'
DEB_NAME = 'libopencv-core-dev_3.2.0+dfsg-4build2_arm64.deb'

SRC_URI[md5sum] = "b3827fdfe3a4cea5c64d113f8c082589"
