inherit native deb-dl

LICENSE = "LGPLv2.1+ & LGPLv2+"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/g/gst-plugins-base1.0/libgstreamer-plugins-base1.0-dev_1.14.5-0ubuntu1~18.04.3_arm64.deb'
DEB_NAME = 'libgstreamer-plugins-base1.0-dev_1.14.5-0ubuntu1~18.04.3_arm64.deb'

SRC_URI[md5sum] = "8e435647a9b652531fdeb5c8aa81fc17"
