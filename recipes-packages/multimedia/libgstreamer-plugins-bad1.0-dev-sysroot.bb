inherit native deb-dl

LICENSE = "GPLv2+ & LGPLv2+ & LGPLv2.1+"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/universe/g/gst-plugins-bad1.0/libgstreamer-plugins-bad1.0-dev_1.14.5-0ubuntu1~18.04.1_arm64.deb'
DEB_NAME = 'libgstreamer-plugins-bad1.0-dev_1.14.5-0ubuntu1~18.04.1_arm64.deb'

SRC_URI[md5sum] = "f795ff9a5b5e898108763d6166a0a6e2"
