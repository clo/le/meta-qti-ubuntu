inherit native deb-dl

LICENSE = "LGPLv2.1+ & LGPLv2+"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/g/gst-plugins-good1.0/libgstreamer-plugins-good1.0-0_1.14.5-0ubuntu1~18.04.3_arm64.deb'
DEB_NAME = 'libgstreamer-plugins-good1.0-0_1.14.5-0ubuntu1~18.04.3_arm64.deb'

SRC_URI[md5sum] = "92f863ae56ee0cd0ca6d4d5791246f8b"
