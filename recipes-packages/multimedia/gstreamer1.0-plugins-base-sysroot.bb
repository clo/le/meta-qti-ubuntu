inherit native deb-dl

LICENSE = "LGPLv2.1+ & LGPLv2+"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/g/gst-plugins-base1.0/gstreamer1.0-plugins-base_1.14.5-0ubuntu1~18.04.3_arm64.deb'
DEB_NAME = 'gstreamer1.0-plugins-base_1.14.5-0ubuntu1~18.04.3_arm64.deb'

SRC_URI[md5sum] = "16a5a5699a9860fb857b21840b497ff9"
