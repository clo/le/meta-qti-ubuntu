inherit native deb-dl

LICENSE = "LGPLv2+"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/universe/g/gst-rtsp-server1.0/libgstrtspserver-1.0-0_1.14.5-0ubuntu1~18.04.1_arm64.deb'
DEB_NAME = 'libgstrtspserver-1.0-0_1.14.5-0ubuntu1~18.04.1_arm64.deb'

SRC_URI[md5sum] = "5d805fad88f4901d00932282abdca63d"

