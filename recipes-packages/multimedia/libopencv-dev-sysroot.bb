LICENSE = "BSD-3-Clause"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/universe/o/opencv/libopencv-dev_3.2.0+dfsg-4ubuntu0.1_arm64.deb'
DEB_NAME = 'libopencv-dev_3.2.0+dfsg-4ubuntu0.1_arm64.deb'

SRC_URI[md5sum] = "2638f51ba14cdba7134d498064cfb377"
