inherit native deb-dl

LICENSE = "GPLv2+ & LGPLv2+ & LGPLv2.1+"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/universe/g/gst-plugins-bad1.0/gstreamer1.0-plugins-bad_1.14.5-0ubuntu1~18.04.1_arm64.deb'
DEB_NAME = 'gstreamer1.0-plugins-bad_1.14.5-0ubuntu1~18.04.1_arm64.deb'

SRC_URI[md5sum] = "b9a4c534171f08b37c14ec1f595f1f29"
