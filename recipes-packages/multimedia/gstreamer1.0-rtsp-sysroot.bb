inherit native deb-dl

LICENSE = "LGPLv2+"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/universe/g/gst-rtsp-server1.0/gstreamer1.0-rtsp_1.14.5-0ubuntu1~18.04.1_arm64.deb'
DEB_NAME = 'gstreamer1.0-rtsp_1.14.5-0ubuntu1~18.04.1_arm64.deb'

SRC_URI[md5sum] = "678fb7c66f5938f9e52c1b81b4fa98a7"
