inherit deb-dl

LICENSE = "TCP-wrappers"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/t/tcp-wrappers/libwrap0_7.6.q-27_arm64.deb'

DEB_NAME = 'libwrap0_7.6.q-27_arm64.deb'

SRC_URI[md5sum] = "cc2bf3b2ed0f0f8ab310f6d2b4b6617f"
