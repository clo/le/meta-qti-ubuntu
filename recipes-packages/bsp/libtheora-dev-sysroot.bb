inherit deb-dl

LICENSE = "BSD-3-Clause"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/libt/libtheora/libtheora-dev_1.1.1+dfsg.1-14_arm64.deb'

DEB_NAME = 'libtheora-dev_1.1.1+dfsg.1-14_arm64.deb'

SRC_URI[md5sum] = "412a5aa53a18c62b68e6083d3f661df2"
