inherit deb-dl

LICENSE = "LGPL-2.1+ & GPL-2.0+"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/r/rtmpdump/librtmp1_2.4+20151223.gitfa8646d.1-1_arm64.deb'

DEB_NAME = 'librtmp1_2.4+20151223.gitfa8646d.1-1_arm64.deb'

SRC_URI[md5sum] = "842ee9e689272c9c55bd4703270a294f"
