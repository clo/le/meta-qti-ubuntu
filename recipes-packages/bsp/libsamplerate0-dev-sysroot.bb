inherit deb-dl

LICENSE = "BSD-2-Clause"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/libs/libsamplerate/libsamplerate0-dev_0.1.9-1_arm64.deb'

DEB_NAME = 'libsamplerate0-dev_0.1.9-1_arm64.deb'

SRC_URI[md5sum] = "364f4b24fbf772e8b472bcd0bb215260"
