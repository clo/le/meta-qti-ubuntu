inherit deb-dl

LICENSE = "GPL-3.0-with-GCC-exception"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/g/gcc-8/libatomic1_8-20180414-1ubuntu2_arm64.deb'

DEB_NAME = 'libatomic1_8-20180414-1ubuntu2_arm64.deb'

SRC_URI[md5sum] = "0b6443440cb6c2d6ebf9158cad89820a"
