inherit deb-dl

LICENSE = "LGPL-2.0+"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/libs/libsoup2.4/libsoup2.4-1_2.62.1-1_arm64.deb'

DEB_NAME = 'libsoup2.4-1_2.62.1-1_arm64.deb'

SRC_URI[md5sum] = "218fa495b12dcaabeca7be2f52c65101"
