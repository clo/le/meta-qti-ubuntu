LICENSE = "GPL-2.0+ & LGPL-2.1+"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/s/systemd/systemd-sysv_237-3ubuntu10_arm64.deb'
DEB_NAME = 'systemd-sysv_237-3ubuntu10_arm64.deb'

SRC_URI[md5sum] = "985209c3eec2b689b111b9e7d5d81c0a"

