inherit deb-dl

LICENSE = "LGPLv2.1+ & MIT-style & BSD-2-Clause & MIT & GPL-2.0+ & GPL-3.0+"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/libu/liburcu/liburcu6_0.10.1-1_arm64.deb'

DEB_NAME = 'liburcu6_0.10.1-1_arm64.deb'

SRC_URI[md5sum] = "7f03f9f03062e3eed43498a4d0f1f15b"
