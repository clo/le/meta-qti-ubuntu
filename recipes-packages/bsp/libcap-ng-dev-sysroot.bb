inherit deb-dl

LICENSE = "LGPL-2.1+ & GPL-3.0"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/libc/libcap-ng/libcap-ng-dev_0.7.7-3.1_arm64.deb'

DEB_NAME = 'libcap-ng-dev_0.7.7-3.1_arm64.deb'

SRC_URI[md5sum] = "f82fec5245fd062eb110055d86acea9b"
