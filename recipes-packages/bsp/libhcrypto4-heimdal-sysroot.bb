inherit deb-dl

LICENSE = "BSD-3-clause & GPL-2.0+"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/h/heimdal/libhcrypto4-heimdal_7.5.0+dfsg-1_arm64.deb'

DEB_NAME = 'libhcrypto4-heimdal_7.5.0+dfsg-1_arm64.deb'

SRC_URI[md5sum] = "6bd5aaec0361b1c94e91d77dd9747533"
