LICENSE = "GPL-2.0+ & GFDL-1.3"

SRC_URI[md5sum] = "e4896a82d996efd3e4fe25700318bfe8"

FULL_LINK = 'http://ports.ubuntu.com/pool/main/libt/libtool/libltdl7_2.4.6-2_arm64.deb'

DEB_NAME = 'libltdl7_2.4.6-2_arm64.deb'
