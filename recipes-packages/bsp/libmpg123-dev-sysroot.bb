inherit deb-dl

LICENSE = "LGPL-2.1"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/m/mpg123/libmpg123-dev_1.25.10-1_arm64.deb'

DEB_NAME = 'libmpg123-dev_1.25.10-1_arm64.deb'

SRC_URI[md5sum] = "749522bc29fc71feefde45aaf3d7cba3"
