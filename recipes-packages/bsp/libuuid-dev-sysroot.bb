inherit native deb-dl

LICENSE = "BSD-3-clause"

SRC_URI[md5sum] = "0a12b1583a46dc4395d052111b771f57"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/u/util-linux/uuid-dev_2.31.1-0.4ubuntu3_arm64.deb'
DEB_NAME = 'uuid-dev_2.31.1-0.4ubuntu3_arm64.deb'
