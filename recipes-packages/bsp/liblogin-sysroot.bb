LICENSE = "BSD-3-Clause & GPL-2.0+"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/s/shadow/login_4.5-1ubuntu1_arm64.deb'
DEB_NAME = 'login_4.5-1ubuntu1_arm64.deb'

SRC_URI[md5sum] = "e3c8d4852c5b21207bafdac4ffdb2fc1"
