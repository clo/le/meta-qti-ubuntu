inherit native deb-dl

LICENSE = "EPL-1.0 & GPL-2.0+"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/c/cross-toolchain-base/libc6-arm64-cross_2.27-3ubuntu1cross1_all.deb'
DEB_NAME = 'libc6-arm64-cross_2.27-3ubuntu1cross1_all.deb'
SRC_URI[md5sum] = "ef1dbe67b4310550bb32820e80608095"
