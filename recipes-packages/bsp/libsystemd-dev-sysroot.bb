inherit deb-dl

LICENSE = "LGPLv2.1+ & GPLv2+ & PD & BSD"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/s/systemd/libsystemd-dev_237-3ubuntu10_arm64.deb'
DEB_NAME = 'libsystemd-dev_237-3ubuntu10_arm64.deb'

SRC_URI[md5sum] = "962824ba3d25280ab4c1c452caed0cc5"
