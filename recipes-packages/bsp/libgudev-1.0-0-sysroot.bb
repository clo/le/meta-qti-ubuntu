inherit deb-dl

LICENSE = "LGPL-2.0+"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/libg/libgudev/libgudev-1.0-0_232-2_arm64.deb'

DEB_NAME = 'libgudev-1.0-0_232-2_arm64.deb'

SRC_URI[md5sum] = "82367a0b317c002f194b03e26b3020d2"
