inherit deb-dl

LICENSE = "LGPL-2.1+ & GPL-3.0+"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/libt/libtasn1-6/libtasn1-6_4.13-2_arm64.deb'

DEB_NAME = 'libtasn1-6_4.13-2_arm64.deb'

SRC_URI[md5sum] = "74c0dc6b9f462892d576ef9ee6d6695d"
