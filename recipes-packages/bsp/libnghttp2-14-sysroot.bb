inherit deb-dl

LICENSE = "MIT"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/n/nghttp2/libnghttp2-14_1.30.0-1ubuntu1_arm64.deb'

DEB_NAME = 'libnghttp2-14_1.30.0-1ubuntu1_arm64.deb'

SRC_URI[md5sum] = "76960a91acb18c752c5c16dc7b666be1"
