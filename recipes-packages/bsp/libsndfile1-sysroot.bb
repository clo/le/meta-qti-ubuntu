inherit deb-dl

LICENSE = "LGPL-2.1"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/libs/libsndfile/libsndfile1_1.0.28-4_arm64.deb'

DEB_NAME = 'libsndfile1_1.0.28-4_arm64.deb'

SRC_URI[md5sum] = "08fdb049f46cf83b39b8b35020a5291b"
