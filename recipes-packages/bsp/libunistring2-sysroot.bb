inherit deb-dl

LICENSE = "(LGPL-3.0+ or GPL-2.0+) & GPL-3.0+"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/libu/libunistring/libunistring2_0.9.9-0ubuntu1_arm64.deb'

DEB_NAME = 'libunistring2_0.9.9-0ubuntu1_arm64.deb'

SRC_URI[md5sum] = "6a2b78ab7d2338d2a7691ce72f261919"
