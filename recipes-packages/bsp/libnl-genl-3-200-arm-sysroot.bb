inherit native deb-dl

LICENSE = "LGPL-2.1"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/libn/libnl3/libnl-genl-3-200_3.2.29-0ubuntu3_arm64.deb'
DEB_NAME = 'libnl-genl-3-200_3.2.29-0ubuntu3_arm64.deb'
SRC_URI[md5sum] = "52f87298f517ec99e49cf9637f2e51fb"