inherit native deb-dl

LICENSE = "BSD-2-Clause & BSD-3-Clause"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/o/orc/liborc-0.4-dev_0.4.28-1_arm64.deb'
DEB_NAME = 'liborc-0.4-dev_0.4.28-1_arm64.deb'
SRC_URI[md5sum] = "f0ace763eaaa6b20e887867b16fd5cb0"
