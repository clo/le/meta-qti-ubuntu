inherit deb-dl

LICENSE = "BSL-1.0 & PD"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/universe/libc/libcrypto++/libcrypto++6_5.6.4-8_arm64.deb'

DEB_NAME = 'libcrypto++6_5.6.4-8_arm64.deb'

SRC_URI[md5sum] = "e0f5ff8cc29c07c67abf51167bab48e1"
