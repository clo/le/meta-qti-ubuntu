inherit deb-dl

LICENSE = "(LGPL-2.1 | MPL-1.1) & BSD-2-Clause"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/t/taglib/libtagc0-dev_1.11.1+dfsg.1-0.2build2_arm64.deb'

DEB_NAME = 'libtagc0-dev_1.11.1+dfsg.1-0.2build2_arm64.deb'

SRC_URI[md5sum] = "b82e160ab3cb50f7281d7dee85fd0356"
