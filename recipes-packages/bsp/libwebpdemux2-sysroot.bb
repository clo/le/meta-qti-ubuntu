inherit deb-dl

LICENSE = "BSD-3-Clause & Apache-2.0"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/libw/libwebp/libwebpdemux2_0.6.1-2_arm64.deb'

DEB_NAME = 'libwebpdemux2_0.6.1-2_arm64.deb'

SRC_URI[md5sum] = "5b894ccbadd9a2126934c4014c0824e4"
