inherit native deb-dl

LICENSE = "LGPL-2.1"

SRC_URI[md5sum] = "ceff45bea5966131c59a8a6f1e6801f0"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/u/util-linux/libblkid1_2.31.1-0.4ubuntu3_arm64.deb'
DEB_NAME = 'libblkid1_2.31.1-0.4ubuntu3_arm64.deb'
