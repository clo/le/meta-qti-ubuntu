inherit native deb-dl

LICENSE = "LGPL-2.1+ & GPL-2.0"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/libs/libsepol/libsepol1-dev_2.7-1_arm64.deb'
DEB_NAME = 'libsepol1-dev_2.7-1_arm64.deb'
SRC_URI[md5sum] = "b82c2d93e18a71427ca14a220224c844"
