inherit deb-dl

LICENSE = "LGPL-2.0+"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/libc/libcroco/libcroco3-dev_0.6.12-2_arm64.deb'

DEB_NAME = 'libcroco3-dev_0.6.12-2_arm64.deb'

SRC_URI[md5sum] = "fdd1197cbc3ccf3d5de6d2cdbf12017d"
