inherit deb-dl

LICENSE = "LGPL-2.1+"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/a/alsa-plugins/libasound2-plugins_1.1.1-1ubuntu1_arm64.deb'

DEB_NAME = 'libasound2-plugins_1.1.1-1ubuntu1_arm64.deb'

SRC_URI[md5sum] = "1f9d2fa8972ada2fffbf98284d7c2f44"
