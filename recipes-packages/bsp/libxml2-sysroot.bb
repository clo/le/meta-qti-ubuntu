inherit deb-dl

LICENSE = "MIT"

SRC_URI[md5sum] = "6d61cac31ac452bea0d1a4e18ae5db77"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/libx/libxml2/libxml2_2.9.4+dfsg1-6.1ubuntu1.9_arm64.deb'
DEB_NAME = 'libxml2_2.9.4+dfsg1-6.1ubuntu1.9_arm64.deb'
