inherit deb-dl

LICENSE = "BSD-3-Clause"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/libv/libvorbis/libvorbis-dev_1.3.5-4.2_arm64.deb'

DEB_NAME = 'libvorbis-dev_1.3.5-4.2_arm64.deb'

SRC_URI[md5sum] = "a7ea0c3d8c70c5696ef10b64e29b0d93"
