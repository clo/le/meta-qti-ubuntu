inherit deb-dl

LICENSE = "BSL-1.0 & PD"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/universe/libc/libcrypto++/libcrypto++-dev_5.6.4-8_arm64.deb'

DEB_NAME = 'libcrypto++-dev_5.6.4-8_arm64.deb'

SRC_URI[md5sum] = "699ee5b3318500f78686bf254c29083e"
