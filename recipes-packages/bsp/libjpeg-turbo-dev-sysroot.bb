inherit deb-dl

LICENSE = "IJG & BSD-3-Clause & Zlib & LGPL-2.1"

SRC_URI[md5sum] = "cdac4140f5d96f9e6cf77f4e5e29a28f"

FULL_LINK = 'http://ports.ubuntu.com/pool/main/libj/libjpeg-turbo/libjpeg-turbo8-dev_1.5.2-0ubuntu5_arm64.deb'

DEB_NAME = 'libjpeg-turbo8-dev_1.5.2-0ubuntu5_arm64.deb'
