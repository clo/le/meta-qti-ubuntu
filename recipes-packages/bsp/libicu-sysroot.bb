inherit deb-dl

LICENSE = "ICU&Unicode-DFS"

SRC_URI[md5sum] = "dff01060f24a27dbae25a1c0d2a09f9d"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/i/icu/libicu60_60.2-3ubuntu3_arm64.deb'
DEB_NAME = 'libicu60_60.2-3ubuntu3_arm64.deb'
