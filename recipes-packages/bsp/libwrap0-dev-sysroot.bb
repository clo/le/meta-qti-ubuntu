inherit deb-dl

LICENSE = "TCP-wrappers"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/t/tcp-wrappers/libwrap0-dev_7.6.q-27_arm64.deb'

DEB_NAME = 'libwrap0-dev_7.6.q-27_arm64.deb'

SRC_URI[md5sum] = "4d51e6cfbe3fb885de1da3893a4f4cc7"
