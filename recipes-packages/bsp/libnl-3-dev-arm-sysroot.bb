inherit native deb-dl

LICENSE = "LGPL-2.1"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/libn/libnl3/libnl-3-dev_3.2.29-0ubuntu3_arm64.deb'
DEB_NAME = 'libnl-3-dev_3.2.29-0ubuntu3_arm64.deb'
SRC_URI[md5sum] = "f154c68dd8372089316a4a883d83558c"