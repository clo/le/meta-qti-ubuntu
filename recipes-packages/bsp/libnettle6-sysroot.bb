inherit deb-dl

LICENSE = "LGPL-2.1+ & PD"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/n/nettle/libnettle6_3.4-1_arm64.deb'

DEB_NAME = 'libnettle6_3.4-1_arm64.deb'

SRC_URI[md5sum] = "cc04b80caff58c493e92bfdb264a5659"
