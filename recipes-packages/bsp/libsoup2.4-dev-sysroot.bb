inherit deb-dl

LICENSE = "LGPL-2.0+"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/libs/libsoup2.4/libsoup2.4-dev_2.62.1-1_arm64.deb'

DEB_NAME = 'libsoup2.4-dev_2.62.1-1_arm64.deb'

SRC_URI[md5sum] = "9c4117bf5144931f551e9fcf9e753dca"
