inherit native deb-dl

LICENSE = "GPL-2.0+ & BSD-3-Clause"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/p/pam/libpam0g_1.1.8-3.6ubuntu2_arm64.deb'
DEB_NAME = 'libpam0g_1.1.8-3.6ubuntu2_arm64.deb'
SRC_URI[md5sum] = "eb02e54138abc86aeca6d1412c73f48b"
