inherit native deb-dl

LICENSE = "EPL-1.0"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/g/gcc-8-cross/libtsan0-arm64-cross_8.4.0-1ubuntu1~18.04cross2_all.deb'
DEB_NAME = 'libtsan0-arm64-cross_8.4.0-1ubuntu1~18.04cross2_all.deb'
SRC_URI[md5sum] = "09a47b08b37bcc190b6562fdc9dd04eb"
