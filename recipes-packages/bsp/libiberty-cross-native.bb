inherit native deb-dl

LICENSE = "GPL-3.0+ & LGPL-2.0+ & GPL-2.0+ & GFDL-1.3 & MIT & PD"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/libi/libiberty/libiberty-dev_20170913-1_arm64.deb'
DEB_NAME = 'libiberty-dev_20170913-1_arm64.deb'
SRC_URI[md5sum] = "60f8c9ae8cf7b1e434e3f38cf9d52235"
