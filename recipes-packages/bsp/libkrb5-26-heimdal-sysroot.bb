inherit deb-dl

LICENSE = "BSD-3-clause & GPL-2.0+"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/h/heimdal/libkrb5-26-heimdal_7.5.0+dfsg-1_arm64.deb'

DEB_NAME = 'libkrb5-26-heimdal_7.5.0+dfsg-1_arm64.deb'

SRC_URI[md5sum] = "aa4d9d1516398c49b78732e53f925016"
