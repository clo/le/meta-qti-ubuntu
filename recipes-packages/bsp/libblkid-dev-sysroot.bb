inherit native deb-dl

LICENSE = "LGPL-2.1"

SRC_URI[md5sum] = "024b3b6b4ea5ddc9d5a57f606ba7bcec"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/u/util-linux/libblkid-dev_2.31.1-0.4ubuntu3_arm64.deb'
DEB_NAME = 'libblkid-dev_2.31.1-0.4ubuntu3_arm64.deb'
