inherit deb-dl

LICENSE = "GPLv2+ & LGPLv2.1+"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/libg/libgcrypt20/libgcrypt20_1.8.1-4ubuntu1_arm64.deb'
DEB_NAME = 'libgcrypt20_1.8.1-4ubuntu1_arm64.deb'

SRC_URI[md5sum] = "ddd5eb3252d9d7337073fe7c7affeecd"
