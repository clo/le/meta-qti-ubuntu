inherit native deb-dl

LICENSE = "EPL-1.0"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/g/gcc-7-cross/libstdc++-7-dev-arm64-cross_7.5.0-3ubuntu1~18.04cross1_all.deb'
DEB_NAME = 'libstdc++-7-dev-arm64-cross_7.5.0-3ubuntu1~18.04cross1_all.deb'
SRC_URI[md5sum] = "3bf407998eaf3d589e79610d35d50aa3"
