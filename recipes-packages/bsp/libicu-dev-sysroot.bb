inherit deb-dl

LICENSE = "ICU&Unicode-DFS"

SRC_URI[md5sum] = "faff88621d2ebd2a342de5d5bd93b960"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/i/icu/libicu-dev_60.2-3ubuntu3_arm64.deb'
DEB_NAME = 'libicu-dev_60.2-3ubuntu3_arm64.deb'
