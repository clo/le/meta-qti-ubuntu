inherit deb-dl

LICENSE = "GPL-2.0+ | AFL-2.1"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/d/dbus/libdbus-1-dev_1.12.2-1ubuntu1_arm64.deb'
DEB_NAME = 'libdbus-1-dev_1.12.2-1ubuntu1_arm64.deb'

SRC_URI[md5sum] = "6c564d0ea9a4707ddeeebb8f1350ad76"
