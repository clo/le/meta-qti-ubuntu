inherit deb-dl

LICENSE = "LGPL-2.0+ & GPL-2.0+"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/libr/librsvg/librsvg2-2_2.40.20-2_arm64.deb'

DEB_NAME = 'librsvg2-2_2.40.20-2_arm64.deb'

SRC_URI[md5sum] = "df68864547097377abc59bf0ce6bf599"
