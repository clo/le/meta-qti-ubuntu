inherit deb-dl

LICENSE = "GPL-3.0+ & LGPL-2.1+"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/g/gnutls28/libgnutls30_3.5.18-1ubuntu1_arm64.deb'

DEB_NAME = 'libgnutls30_3.5.18-1ubuntu1_arm64.deb'

SRC_URI[md5sum] = "43f94efc7dc1db7dbd0f5a22bac25efd"
