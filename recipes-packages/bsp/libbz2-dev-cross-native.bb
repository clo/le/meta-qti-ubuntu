inherit native deb-dl

LICENSE = "GPL-2.0 & bzip2-1.0.6"

SRC_URI[md5sum] = "c2f7e00e17e7fc7c108e01e31c73c305"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/b/bzip2/libbz2-dev_1.0.6-8.1_arm64.deb'
DEB_NAME = 'libbz2-dev_1.0.6-8.1_arm64.deb'
