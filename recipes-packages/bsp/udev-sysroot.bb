inherit deb-dl

LICENSE = "GPL-2.0+ & LGPL-2.1+"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/s/systemd/udev_237-3ubuntu10_arm64.deb'
DEB_NAME = 'udev_237-3ubuntu10_arm64.deb'

SRC_URI[md5sum] = "19f387fa9713fc24fc242a31530adf57"
