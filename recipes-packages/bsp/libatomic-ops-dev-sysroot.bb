inherit deb-dl

LICENSE = "GPL-2.0 & MIT"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/liba/libatomic-ops/libatomic-ops-dev_7.6.2-1_arm64.deb'

DEB_NAME = 'libatomic-ops-dev_7.6.2-1_arm64.deb'

SRC_URI[md5sum] = "1a477d516ca0593929a3063b6228851d"
