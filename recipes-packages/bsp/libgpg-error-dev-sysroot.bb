inherit deb-dl

LICENSE = "GPLv2+ & LGPLv2.1+"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/libg/libgpg-error/libgpg-error-dev_1.27-6_arm64.deb'
DEB_NAME = 'libgpg-error-dev_1.27-6_arm64.deb'

SRC_URI[md5sum] = "0b00ecbc56dc677cf0622ceb61022ebc"
