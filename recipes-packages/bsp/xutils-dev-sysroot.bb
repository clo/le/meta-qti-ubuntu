inherit deb-dl

LICENSE = "X11"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/universe/x/xutils-dev/xutils-dev_7.7+5ubuntu1_arm64.deb'

DEB_NAME = 'xutils-dev_7.7+5ubuntu1_arm64.deb'

SRC_URI[md5sum] = "a5da5cad9c7faeadac805d9cdf1e5855"
