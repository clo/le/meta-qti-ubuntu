inherit deb-dl

LICENSE = "(LGPL-3.0+ or GPL-2.0+) & GPL-3.0+"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/libu/libunistring/libunistring-dev_0.9.9-0ubuntu1_arm64.deb'

DEB_NAME = 'libunistring-dev_0.9.9-0ubuntu1_arm64.deb'

SRC_URI[md5sum] = "87d08546f69814e82d1f6d35e35a7d94"
