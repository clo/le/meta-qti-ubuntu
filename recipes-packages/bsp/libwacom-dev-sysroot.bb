inherit deb-dl

LICENSE = "MIT"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/libw/libwacom/libwacom-dev_0.29-1_arm64.deb'

DEB_NAME = 'libwacom-dev_0.29-1_arm64.deb'

SRC_URI[md5sum] = "0e6e2518e5273a3940254b11f1c3185f"
