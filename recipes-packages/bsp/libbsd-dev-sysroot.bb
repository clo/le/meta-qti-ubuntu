inherit deb-dl

LICENSE = "BSD & MIT & ISC & Beerware & PD"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/libb/libbsd/libbsd-dev_0.8.7-1_arm64.deb'

DEB_NAME = 'libbsd-dev_0.8.7-1_arm64.deb'

SRC_URI[md5sum] = "990e28c0397e52b3f6769b11bd8e0d90"
