inherit deb-dl

LICENSE = "curl"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/c/curl/libcurl4-openssl-dev_7.58.0-2ubuntu3_arm64.deb'

DEB_NAME = 'libcurl4-openssl-dev_7.58.0-2ubuntu3_arm64.deb'

SRC_URI[md5sum] = "9e5b15bf335b78595d14fb511642d3ea"
