inherit native deb-dl

LICENSE = "EPL-1.0"

SRC_URI[md5sum] = "b187ab34c1bf13a0ee772f82dd531cab"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/g/gcc-8-cross/libatomic1-arm64-cross_8.4.0-1ubuntu1~18.04cross2_all.deb'
DEB_NAME = 'libatomic1-arm64-cross_8.4.0-1ubuntu1~18.04cross2_all.deb'
