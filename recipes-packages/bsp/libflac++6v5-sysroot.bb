inherit deb-dl

LICENSE = "GFDL-1.1+ & GPL-2.0+ & LGPL-2.1+ & BSD-3-Clause & PD & ISC"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/f/flac/libflac++6v5_1.3.2-1_arm64.deb'

DEB_NAME = 'libflac++6v5_1.3.2-1_arm64.deb'

SRC_URI[md5sum] = "3470fef0f9520d757f6a481b54ad2033"
