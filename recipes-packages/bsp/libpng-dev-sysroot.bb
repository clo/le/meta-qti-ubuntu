inherit deb-dl

LICENSE = "Libpng"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/libp/libpng1.6/libpng-dev_1.6.34-1_arm64.deb'
DEB_NAME = 'libpng-dev_1.6.34-1_arm64.deb'

SRC_URI[md5sum] = "e70232b9aa7ad435d0627817f4d66438"
