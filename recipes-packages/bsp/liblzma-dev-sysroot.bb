inherit deb-dl

LICENSE = "GPLv2+ & LGPLv2.1+"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/x/xz-utils/liblzma-dev_5.2.2-1.3_arm64.deb'
DEB_NAME = 'liblzma-dev_5.2.2-1.3_arm64.deb'

SRC_URI[md5sum] = "1106429565a24ca1a6221fc59c232dca"
