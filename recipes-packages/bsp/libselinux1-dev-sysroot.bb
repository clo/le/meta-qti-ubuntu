inherit native deb-dl

LICENSE = "PD & LGPL-2.1+ & GPL-2.0"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/libs/libselinux/libselinux1-dev_2.7-2build2_arm64.deb'
DEB_NAME = 'libselinux1-dev_2.7-2build2_arm64.deb'
SRC_URI[md5sum] = "d7534a58701e22678cbd962612ac80fa"
