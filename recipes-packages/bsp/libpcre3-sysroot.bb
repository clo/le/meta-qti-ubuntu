inherit native deb-dl

LICENSE = "BSD-3-Clause"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/p/pcre3/libpcre3_8.39-9_arm64.deb'
DEB_NAME = 'libpcre3_8.39-9_arm64.deb'
SRC_URI[md5sum] = "3ae4ed7c50ee0724a99123e26a5c65b1"
