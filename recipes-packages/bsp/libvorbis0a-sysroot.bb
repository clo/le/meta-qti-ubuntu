inherit deb-dl

LICENSE = "BSD-3-Clause"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/libv/libvorbis/libvorbis0a_1.3.5-4.2_arm64.deb'

DEB_NAME = 'libvorbis0a_1.3.5-4.2_arm64.deb'

SRC_URI[md5sum] = "64803ce3128e3befc43e5f5d46f477c2"
