inherit deb-dl

LICENSE = "MIT & PD & GPL-3.0+"

SRC_URI[md5sum] = "eb426db54320796f114944c59c89e190"

FULL_LINK = 'http://ports.ubuntu.com/pool/main/libj/libjsoncpp/libjsoncpp-dev_1.7.4-3_arm64.deb'

DEB_NAME = 'libjsoncpp-dev_1.7.4-3_arm64.deb'
