inherit deb-dl

LICENSE = "(GPL-2.0+ | LGPL-3.0+) & GPL-3.0+"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/libi/libidn2/libidn2-0_2.0.4-1.1build2_arm64.deb'

DEB_NAME = 'libidn2-0_2.0.4-1.1build2_arm64.deb'

SRC_URI[md5sum] = "4067bc5d636a8ef1028c5b1b54a7d53d"
