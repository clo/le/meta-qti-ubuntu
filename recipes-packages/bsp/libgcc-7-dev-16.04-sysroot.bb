
inherit native deb-dl

LICENSE = "EPL-1.0"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/g/gcc-7-cross/libgcc-7-dev-arm64-cross_7.3.0-16ubuntu3cross1_all.deb'
DEB_NAME = 'libgcc-7-dev-arm64-cross_7.3.0-16ubuntu3cross1_all.deb'
SRC_URI[md5sum] = "da9e9c15a03ebd1b739e4d85e3c996a7"
