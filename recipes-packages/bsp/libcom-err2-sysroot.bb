inherit deb-dl

LICENSE = "GPL-2.0 & LGPL-2.0 & BSD-3-Clause & MIT"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/e/e2fsprogs/libcom-err2_1.44.1-1_arm64.deb'

DEB_NAME = 'libcom-err2_1.44.1-1_arm64.deb'

SRC_URI[md5sum] = "ba439cb987235124db6735a22a206a47"
