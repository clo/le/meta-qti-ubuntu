inherit deb-dl

LICENSE = "GPL-3.0+ & MIT & MIT-style"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/libp/libpciaccess/libpciaccess0_0.14-1_arm64.deb'

DEB_NAME = 'libpciaccess0_0.14-1_arm64.deb'

SRC_URI[md5sum] = "741ff7eaae81dac69765aa0f3d522cec"
