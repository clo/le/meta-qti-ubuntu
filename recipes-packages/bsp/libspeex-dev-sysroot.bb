inherit deb-dl

LICENSE = "BSD-3-Clause"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/s/speex/libspeex-dev_1.2~rc1.2-1ubuntu2_arm64.deb'

DEB_NAME = 'libspeex-dev_1.2~rc1.2-1ubuntu2_arm64.deb'

SRC_URI[md5sum] = "bcd519557dd113b3ebb94d93aba4ae34"
