inherit deb-dl

LICENSE = "ICU&Unicode-DFS"

SRC_URI[md5sum] = "003e65fd1a48c962b3144e6680b0bc7d"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/i/icu/libiculx60_60.2-3ubuntu3_arm64.deb'
DEB_NAME = 'libiculx60_60.2-3ubuntu3_arm64.deb'
