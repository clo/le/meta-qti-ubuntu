inherit deb-dl

LICENSE = "LGPL-2.1"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/m/mpg123/libout123-0_1.25.10-1_arm64.deb'

DEB_NAME = 'libout123-0_1.25.10-1_arm64.deb'

SRC_URI[md5sum] = "605496a5a9f01fb5f2dcae88a7ada6be"
