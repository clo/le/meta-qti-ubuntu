inherit native deb-dl

LICENSE = "EPL-1.0"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/g/gcc-8-cross/libstdc++6-arm64-cross_8.4.0-1ubuntu1~18.04cross2_all.deb'
DEB_NAME = 'libstdc++6-arm64-cross_8.4.0-1ubuntu1~18.04cross2_all.deb'
SRC_URI[md5sum] = "ea3be0efedc0532bfcc12cf99e19d8bd"
