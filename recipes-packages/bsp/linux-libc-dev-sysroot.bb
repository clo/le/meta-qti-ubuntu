inherit native deb-dl

LICENSE = "LGPL-2.1"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/l/linux/linux-libc-dev_4.15.0-20.21_arm64.deb'
DEB_NAME = 'linux-libc-dev_4.15.0-20.21_arm64.deb'
SRC_URI[md5sum] = "b6b7011eca728c0ea6d0fd1c2be62ee8"
