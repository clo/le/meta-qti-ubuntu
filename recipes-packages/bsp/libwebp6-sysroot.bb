inherit deb-dl

LICENSE = "BSD-3-Clause & Apache-2.0"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/libw/libwebp/libwebp6_0.6.1-2_arm64.deb'

DEB_NAME = 'libwebp6_0.6.1-2_arm64.deb'

SRC_URI[md5sum] = "f62098e26756d5f21319da94ce0843f0"
