inherit deb-dl

LICENSE = "LGPL-2.1+ & PD"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/n/nettle/libhogweed4_3.4-1_arm64.deb'

DEB_NAME = 'libhogweed4_3.4-1_arm64.deb'

SRC_URI[md5sum] = "164c78089dd6f8e28e5c4864d21264cb"
