inherit deb-dl

LICENSE = "GPL-2.0+"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/universe/libi/libid3tag/libid3tag0_0.15.1b-13_arm64.deb'

DEB_NAME = 'libid3tag0_0.15.1b-13_arm64.deb'

SRC_URI[md5sum] = "49440901009b8006ac957e8bf061e66b"
