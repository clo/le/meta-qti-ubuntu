inherit deb-dl

LICENSE = "LGPL-2.0+"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/libg/libgudev/libgudev-1.0-dev_232-2_arm64.deb'

DEB_NAME = 'libgudev-1.0-dev_232-2_arm64.deb'

SRC_URI[md5sum] = "7b62256a2fcfa952bcc65ccb0429589d"
