inherit deb-dl

LICENSE = "MIT"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/libw/libwacom/libwacom2_0.29-1_arm64.deb'

DEB_NAME = 'libwacom2_0.29-1_arm64.deb'

SRC_URI[md5sum] = "6c741884e13cb43c9495a324c3f61796"
