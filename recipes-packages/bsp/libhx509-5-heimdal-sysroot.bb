inherit deb-dl

LICENSE = "BSD-3-clause & GPL-2.0+"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/h/heimdal/libhx509-5-heimdal_7.5.0+dfsg-1_arm64.deb'

DEB_NAME = 'libhx509-5-heimdal_7.5.0+dfsg-1_arm64.deb'

SRC_URI[md5sum] = "b096eb36b8fb60ba322d490fb1e65563"
