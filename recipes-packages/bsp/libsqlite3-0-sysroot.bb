inherit deb-dl

LICENSE = "PD & GPL-2.0+"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/s/sqlite3/libsqlite3-0_3.22.0-1_arm64.deb'

DEB_NAME = 'libsqlite3-0_3.22.0-1_arm64.deb'

SRC_URI[md5sum] = "dc409d0be69e473695a588b61cc21fb3"
