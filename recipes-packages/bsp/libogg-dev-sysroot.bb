inherit deb-dl

LICENSE = "BSD-3-Clause"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/libo/libogg/libogg-dev_1.3.2-1_arm64.deb'

DEB_NAME = 'libogg-dev_1.3.2-1_arm64.deb'

SRC_URI[md5sum] = "833db325ab2e278c5b94e949cad1be26"
