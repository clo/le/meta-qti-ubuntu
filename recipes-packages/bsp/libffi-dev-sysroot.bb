inherit native deb-dl

LICENSE = "MIT"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/libf/libffi/libffi-dev_3.2.1-8_arm64.deb'
DEB_NAME = 'libffi-dev_3.2.1-8_arm64.deb'
SRC_URI[md5sum] = "61dc01684e92f445a5a76566991ce93a"
