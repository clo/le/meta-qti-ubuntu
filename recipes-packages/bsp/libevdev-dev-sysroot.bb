inherit deb-dl

LICENSE = "X11 & GPL-2.0+"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/libe/libevdev/libevdev-dev_1.5.8+dfsg-1_arm64.deb'
DEB_NAME = 'libevdev-dev_1.5.8+dfsg-1_arm64.deb'

SRC_URI[md5sum] = "35e4a4fcd953970c667bf5e5fc2e127d"

