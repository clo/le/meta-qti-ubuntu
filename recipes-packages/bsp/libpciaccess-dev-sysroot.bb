inherit deb-dl

LICENSE = "GPL-3.0+ & MIT & MIT-style "

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/libp/libpciaccess/libpciaccess-dev_0.14-1_arm64.deb'

DEB_NAME = 'libpciaccess-dev_0.14-1_arm64.deb'

SRC_URI[md5sum] = "4cfaf737dd43b94e1df55fd12eda0d15"
