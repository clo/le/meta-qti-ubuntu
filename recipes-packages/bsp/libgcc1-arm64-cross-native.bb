inherit native deb-dl

LICENSE = "EPL-1.0"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/g/gcc-8-cross/libgcc1-arm64-cross_8.4.0-1ubuntu1~18.04cross2_all.deb'
DEB_NAME = 'libgcc1-arm64-cross_8.4.0-1ubuntu1~18.04cross2_all.deb'
SRC_URI[md5sum] = "22d28a1cdfd077ad9368c527b76d6379"
