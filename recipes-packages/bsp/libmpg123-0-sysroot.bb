inherit deb-dl

LICENSE = "LGPL-2.1"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/m/mpg123/libmpg123-0_1.25.10-1_arm64.deb'

DEB_NAME = 'libmpg123-0_1.25.10-1_arm64.deb'

SRC_URI[md5sum] = "6520568a6152ae6c6bafeb88f6f574a9"
