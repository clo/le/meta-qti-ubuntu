inherit deb-dl

LICENSE = "BSD-3-Clause & Apache-2.0"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/libw/libwebp/libwebpmux3_0.6.1-2_arm64.deb'

DEB_NAME = 'libwebpmux3_0.6.1-2_arm64.deb'

SRC_URI[md5sum] = "a1bd0c77d57928c8caa38da5297c6ebf"
