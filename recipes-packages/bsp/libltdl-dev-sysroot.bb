LICENSE = "GPL-2.0+ & GFDL-1.3"

SRC_URI[md5sum] = "f6ac9b5b0874cb30e1ba4c147a452af6"

FULL_LINK = 'http://ports.ubuntu.com/pool/main/libt/libtool/libltdl-dev_2.4.6-2_arm64.deb'

DEB_NAME = 'libltdl-dev_2.4.6-2_arm64.deb'
