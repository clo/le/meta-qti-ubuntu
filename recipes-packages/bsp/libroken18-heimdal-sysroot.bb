inherit deb-dl

LICENSE = "BSD-3-clause & GPL-2.0+"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/h/heimdal/libroken18-heimdal_7.5.0+dfsg-1_arm64.deb'

DEB_NAME = 'libroken18-heimdal_7.5.0+dfsg-1_arm64.deb'

SRC_URI[md5sum] = "0ef881281f5f867c1acf18d5ec551cc3"
