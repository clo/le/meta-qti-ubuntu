inherit deb-dl

LICENSE = "MIT & PD & GPL-3.0+"

SRC_URI[md5sum] = "4abac907faeb27b1753e20c5bab7bb9c"

FULL_LINK = 'http://ports.ubuntu.com/pool/main/libj/libjsoncpp/libjsoncpp1_1.7.4-3_arm64.deb'

DEB_NAME = 'libjsoncpp1_1.7.4-3_arm64.deb'
