inherit deb-dl

LICENSE = "BSD-3-Clause"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/libv/libvorbis/libvorbisfile3_1.3.5-4.2_arm64.deb'

DEB_NAME = 'libvorbisfile3_1.3.5-4.2_arm64.deb'

SRC_URI[md5sum] = "1dc9ab028b358b88dea2bffa49b0906b"
