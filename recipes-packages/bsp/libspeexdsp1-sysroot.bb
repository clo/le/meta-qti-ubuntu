inherit deb-dl

LICENSE = "BSD-3-Clause"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/s/speex/libspeexdsp1_1.2~rc1.2-1ubuntu2_arm64.deb'

DEB_NAME = 'libspeexdsp1_1.2~rc1.2-1ubuntu2_arm64.deb'

SRC_URI[md5sum] = "f347a02a4d7384d6a2ea072bba3425ee"
