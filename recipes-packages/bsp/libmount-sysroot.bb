inherit native deb-dl

LICENSE = "LGPL-2.1+ & LGPL-3.0"

SRC_URI[md5sum] = "f8d8f1ddbf5c22129c23244cb4b9a50c"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/u/util-linux/libmount1_2.31.1-0.4ubuntu3_arm64.deb'
DEB_NAME = 'libmount1_2.31.1-0.4ubuntu3_arm64.deb'
