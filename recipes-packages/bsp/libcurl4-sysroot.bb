inherit deb-dl

LICENSE = "curl"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/c/curl/libcurl4_7.58.0-2ubuntu3_arm64.deb'

DEB_NAME = 'libcurl4_7.58.0-2ubuntu3_arm64.deb'

SRC_URI[md5sum] = "79103c21a035932bff3d0c5cc57ecbba"
