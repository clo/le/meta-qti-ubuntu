inherit deb-dl

LICENSE = "MIT"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/j/json-c/libjson-c-dev_0.12.1-1.3_arm64.deb'

DEB_NAME = 'libjson-c-dev_0.12.1-1.3_arm64.deb'

SRC_URI[md5sum] = "3c3770063b1bd64e407375df383aa868"
