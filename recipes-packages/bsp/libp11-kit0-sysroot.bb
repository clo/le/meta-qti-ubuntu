inherit deb-dl

LICENSE = "BSD-3-Clause"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/p/p11-kit/libp11-kit0_0.23.9-2_arm64.deb'

DEB_NAME = 'libp11-kit0_0.23.9-2_arm64.deb'

SRC_URI[md5sum] = "b31865b87dea8afb58d400224baf899d"
