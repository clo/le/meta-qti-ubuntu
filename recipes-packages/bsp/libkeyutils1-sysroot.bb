inherit deb-dl

LICENSE = "GPL-2.0+ & LGPL-2.0+"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/k/keyutils/libkeyutils1_1.5.9-9.2ubuntu2_arm64.deb'

DEB_NAME = 'libkeyutils1_1.5.9-9.2ubuntu2_arm64.deb'

SRC_URI[md5sum] = "8fe900225d6a766030a3126ba5f41d78"
