inherit deb-dl

LICENSE = "GPL-2.0+ & LGPL-2.1+"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/s/systemd/libudev-dev_237-3ubuntu10_arm64.deb'
DEB_NAME = 'libudev-dev_237-3ubuntu10_arm64.deb'

SRC_URI[md5sum] = "15560f08e29b70f71e04840b4593517a"
