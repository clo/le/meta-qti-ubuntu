inherit deb-dl

LICENSE = "GPL-2.0+ & LGPL-2.1+"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/s/systemd/libudev1_237-3ubuntu10_arm64.deb'
DEB_NAME = 'libudev1_237-3ubuntu10_arm64.deb'

SRC_URI[md5sum] = "cdf1856b9b8459c687538da87bef09e7"
