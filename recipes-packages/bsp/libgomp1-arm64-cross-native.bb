inherit native deb-dl

LICENSE = "EPL-1.0"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/g/gcc-8-cross/libgomp1-arm64-cross_8.4.0-1ubuntu1~18.04cross2_all.deb'
DEB_NAME = 'libgomp1-arm64-cross_8.4.0-1ubuntu1~18.04cross2_all.deb'
SRC_URI[md5sum] = "bf0b320a553b0f89d0d07dd2ef483aa6"
