inherit native deb-dl

LICENSE = "LGPL-2.1+ & GPL-2.0"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/libs/libsepol/libsepol1_2.7-1_arm64.deb'
DEB_NAME = 'libsepol1_2.7-1_arm64.deb'
SRC_URI[md5sum] = "c256817961eab39b515820feb393bfc9"
