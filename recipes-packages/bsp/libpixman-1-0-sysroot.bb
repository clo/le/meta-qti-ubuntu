inherit deb-dl

LICENSE = "MIT"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/p/pixman/libpixman-1-0_0.34.0-2_arm64.deb'

DEB_NAME = 'libpixman-1-0_0.34.0-2_arm64.deb'

SRC_URI[md5sum] = "5987d0b658627a2360b7dd8757f13236"
