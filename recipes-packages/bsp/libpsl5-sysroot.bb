inherit deb-dl

LICENSE = "MIT & BSD-3-Clause"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/libp/libpsl/libpsl5_0.19.1-5build1_arm64.deb'

DEB_NAME = 'libpsl5_0.19.1-5build1_arm64.deb'

SRC_URI[md5sum] = "97a57a3618f9ea8a530e28fe4043acd6"
