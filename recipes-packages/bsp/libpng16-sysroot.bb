inherit native deb-dl

LICENSE = "Libpng"

SRC_URI[md5sum] = "8afc1ce6024181c1e235f95cf356b29a"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/libp/libpng1.6/libpng16-16_1.6.34-1_arm64.deb'
DEB_NAME = 'libpng16-16_1.6.34-1_arm64.deb'
