inherit deb-dl

LICENSE = "GPLv2+ & LGPLv2.1+"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/x/xz-utils/liblzma5_5.2.2-1.3_arm64.deb'
DEB_NAME = 'liblzma5_5.2.2-1.3_arm64.deb'

SRC_URI[md5sum] = "feddbbcef258e02ec1912e7a69a4f071"
