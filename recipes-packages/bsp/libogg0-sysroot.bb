inherit deb-dl

LICENSE = "BSD-3-Clause"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/libo/libogg/libogg0_1.3.2-1_arm64.deb'

DEB_NAME = 'libogg0_1.3.2-1_arm64.deb'

SRC_URI[md5sum] = "5d25fe22627426430ead6416a7bf194f"
