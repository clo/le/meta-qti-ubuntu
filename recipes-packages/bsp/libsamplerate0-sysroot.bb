inherit deb-dl

LICENSE = "BSD-2-Clause"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/libs/libsamplerate/libsamplerate0_0.1.9-1_arm64.deb'

DEB_NAME = 'libsamplerate0_0.1.9-1_arm64.deb'

SRC_URI[md5sum] = "9732507fae8bc076f180dfd3bfe9ed46"
