inherit deb-dl

LICENSE = "MIT"

SRC_URI[md5sum] = "aabe7ab8e986e57a3217b2d7a5bc63ad"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/libx/libxml2/libxml2-dev_2.9.4+dfsg1-6.1ubuntu1.9_arm64.deb'
DEB_NAME = 'libxml2-dev_2.9.4+dfsg1-6.1ubuntu1.9_arm64.deb'
