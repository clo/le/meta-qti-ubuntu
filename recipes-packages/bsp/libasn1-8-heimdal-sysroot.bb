inherit deb-dl

LICENSE = "BSD-3-clause & GPL-2.0+"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/h/heimdal/libasn1-8-heimdal_7.5.0+dfsg-1_arm64.deb'

DEB_NAME = 'libasn1-8-heimdal_7.5.0+dfsg-1_arm64.deb'

SRC_URI[md5sum] = "cb14392f05e7d21f8bea3dc3f2154a3e"
