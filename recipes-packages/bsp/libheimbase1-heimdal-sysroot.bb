inherit deb-dl

LICENSE = "BSD-3-clause & GPL-2.0+"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/h/heimdal/libheimbase1-heimdal_7.5.0+dfsg-1_arm64.deb'

DEB_NAME = 'libheimbase1-heimdal_7.5.0+dfsg-1_arm64.deb'

SRC_URI[md5sum] = "0d3f534aa9ff1be037f9d2d920394825"
