inherit native deb-dl

LICENSE = "BSD-2-Clause & BSD-3-Clause"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/o/orc/liborc-0.4-0_0.4.28-1_arm64.deb'
DEB_NAME = 'liborc-0.4-0_0.4.28-1_arm64.deb'
SRC_URI[md5sum] = "5117b695aed739ff1c2a7b5a6397fed9"
