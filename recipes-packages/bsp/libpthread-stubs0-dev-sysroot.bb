inherit deb-dl

LICENSE = "MIT"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/libp/libpthread-stubs/libpthread-stubs0-dev_0.3-4_arm64.deb'

DEB_NAME = 'libpthread-stubs0-dev_0.3-4_arm64.deb'

SRC_URI[md5sum] = "5b836110f3d8301c6ef7078f0c9f09b0"
