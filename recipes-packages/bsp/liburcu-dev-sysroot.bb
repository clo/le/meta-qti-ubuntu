inherit deb-dl

LICENSE = "LGPLv2.1+ & MIT-style & BSD-2-Clause & MIT & GPL-2.0+ & GPL-3.0+"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/libu/liburcu/liburcu-dev_0.10.1-1_arm64.deb'

DEB_NAME = 'liburcu-dev_0.10.1-1_arm64.deb'

SRC_URI[md5sum] = "7fc2956bec653742df0d938e552d22d4"
