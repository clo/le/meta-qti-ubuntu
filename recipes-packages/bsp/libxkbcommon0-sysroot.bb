inherit deb-dl

LICENSE = "MIT & MIT-style"

SRC_URI[md5sum] = "63b652f46c5c0a5c9ffc3f04757545ef"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/libx/libxkbcommon/libxkbcommon0_0.8.0-1ubuntu0.1_arm64.deb'
DEB_NAME = 'libxkbcommon0_0.8.0-1ubuntu0.1_arm64.deb'
