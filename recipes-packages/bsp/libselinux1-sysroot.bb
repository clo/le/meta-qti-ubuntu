inherit native deb-dl

LICENSE = "PD & LGPL-2.1+ & GPL-2.0"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/libs/libselinux/libselinux1_2.7-2build2_arm64.deb'
DEB_NAME = 'libselinux1_2.7-2build2_arm64.deb'
SRC_URI[md5sum] = "45a3bc2b0bb361278b6527e62d2c0441"
