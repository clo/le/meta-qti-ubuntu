inherit deb-dl

LICENSE = "BSD-3-clause & GPL-2.0+"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/h/heimdal/libwind0-heimdal_7.5.0+dfsg-1_arm64.deb'

DEB_NAME = 'libwind0-heimdal_7.5.0+dfsg-1_arm64.deb'

SRC_URI[md5sum] = "8280187f7e5cb03176646877e6424555"
