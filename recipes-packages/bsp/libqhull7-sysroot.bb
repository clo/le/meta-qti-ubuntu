inherit deb-dl

LICENSE = "Qhull & GPL-3.0+"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/universe/q/qhull/libqhull7_2015.2-4_arm64.deb'

DEB_NAME = 'libqhull7_2015.2-4_arm64.deb'

SRC_URI[md5sum] = "16c6edd259c3785c1765c763a99e9b68"
