inherit native deb-dl

LICENSE = "LGPL-2.1 & GPL-2.0+ & GPL-3.0"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/libc/libcap-ng/libcap-ng0_0.7.7-3.1_arm64.deb'
DEB_NAME = 'libcap-ng0_0.7.7-3.1_arm64.deb'
SRC_URI[md5sum] = "96eac736278a3e78cfd296bca20dd132"
