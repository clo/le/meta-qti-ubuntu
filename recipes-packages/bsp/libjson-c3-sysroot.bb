inherit deb-dl

LICENSE = "MIT"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/j/json-c/libjson-c3_0.12.1-1.3_arm64.deb'

DEB_NAME = 'libjson-c3_0.12.1-1.3_arm64.deb'

SRC_URI[md5sum] = "fad375a216de5a1b39dfabf43f623636"
