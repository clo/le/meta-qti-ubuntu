inherit native deb-dl

LICENSE = "LGPL-2.1 & GPL-2.0"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/a/audit/libaudit1_2.8.2-1ubuntu1_arm64.deb'
DEB_NAME = 'libaudit1_2.8.2-1ubuntu1_arm64.deb'

SRC_URI[md5sum] = "d5df25fd0d8769a3431df5c9e0bbc5b8"
