inherit deb-dl

LICENSE = "LGPLv2.1+ & GPLv2+ & PD & BSD"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/s/systemd/libsystemd0_237-3ubuntu10_arm64.deb'
DEB_NAME = 'libsystemd0_237-3ubuntu10_arm64.deb'

SRC_URI[md5sum] = "fd5413e670ed10a1d3d2cdfb6286f4fd"
