inherit deb-dl

LICENSE = "BSD-3-Clause & Apache-2.0"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/libw/libwebp/libwebp-dev_0.6.1-2_arm64.deb'

DEB_NAME = 'libwebp-dev_0.6.1-2_arm64.deb'

SRC_URI[md5sum] = "66d14e5175b9488273b078dd9903da96"
