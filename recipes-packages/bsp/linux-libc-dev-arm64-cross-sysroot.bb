
inherit native deb-dl

LICENSE = "EPL-1.0"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/c/cross-toolchain-base/linux-libc-dev-arm64-cross_4.15.0-18.19cross1_all.deb'
DEB_NAME = 'linux-libc-dev-arm64-cross_4.15.0-18.19cross1_all.deb'
SRC_URI[md5sum] = "33ef65240b137de71a3a9c2a8f692bff"
