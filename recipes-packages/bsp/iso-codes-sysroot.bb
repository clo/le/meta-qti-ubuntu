inherit deb-dl

LICENSE = "LGPL-2.1+"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/i/iso-codes/iso-codes_3.79-1_all.deb'

DEB_NAME = 'iso-codes_3.79-1_all.deb'

SRC_URI[md5sum] = "3251ba2ec506edb3dfd87a04c52169bc"
