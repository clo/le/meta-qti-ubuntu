inherit deb-dl

LICENSE = "GPLv2 & BSD-2-Clause"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/l/lz4/liblz4-1_0.0~r131-2ubuntu3_arm64.deb'
DEB_NAME = 'liblz4-1_0.0~r131-2ubuntu3_arm64.deb'

SRC_URI[md5sum] = "ff01bc732ab4eb729bc82c149e45f6f3"
