inherit native deb-dl

LICENSE = "EPL-1.0"

SRC_URI[md5sum] = "b074a10c2e441bbdc5a2eb479825a136"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/g/gcc-7-cross/libasan4-arm64-cross_7.5.0-3ubuntu1~18.04cross1_all.deb'
DEB_NAME = 'libasan4-arm64-cross_7.5.0-3ubuntu1~18.04cross1_all.deb'
