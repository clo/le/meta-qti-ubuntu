inherit deb-dl

LICENSE = "MIT"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/m/mtdev/libmtdev-dev_1.1.5-1ubuntu3_arm64.deb'

DEB_NAME = 'libmtdev-dev_1.1.5-1ubuntu3_arm64.deb'

SRC_URI[md5sum] = "d5172486e9e14b5185d6f9a7901fa809"
