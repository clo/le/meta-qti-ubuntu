inherit deb-dl

LICENSE = "GPLv2+ & LGPLv2.1+"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/libg/libgcrypt20/libgcrypt20-dev_1.8.1-4ubuntu1_arm64.deb'
DEB_NAME = 'libgcrypt20-dev_1.8.1-4ubuntu1_arm64.deb'

SRC_URI[md5sum] = "73567394e911279e4e72f136e5670742"
