inherit deb-dl

LICENSE = "MIT & BSD-3-Clause & more"
#For complete LICENSE info refer to this file http://changelogs.ubuntu.com/changelogs/pool/main/k/krb5/krb5_1.16-2ubuntu0.1/copyright

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/k/krb5/libkrb5-3_1.16-2build1_arm64.deb'

DEB_NAME = 'libkrb5-3_1.16-2build1_arm64.deb'

SRC_URI[md5sum] = "24c243caeaca71c773706d68122f6481"
