inherit native deb-dl

LICENSE = "MIT & BSD-3-Clause"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/n/ncurses/libtinfo-dev_6.1-1ubuntu1_arm64.deb'
DEB_NAME = 'libtinfo-dev_6.1-1ubuntu1_arm64.deb'
SRC_URI[md5sum] = "5fcd6a2b460a4584f42bba654dccd8ea"
