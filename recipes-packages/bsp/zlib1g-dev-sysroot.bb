LICENSE = "zlib"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/z/zlib/zlib1g-dev_1.2.11.dfsg-0ubuntu2_arm64.deb'
DEB_NAME = 'zlib1g-dev_1.2.11.dfsg-0ubuntu2_arm64.deb'

SRC_URI[md5sum] = "6059cb3427e7f3cad6383f3f5bd41207"
