inherit deb-dl

LICENSE = "GFDL-1.1+ & GPL-2.0+ & LGPL-2.1+ & BSD-3-Clause & PD & ISC"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/f/flac/libflac8_1.3.2-1_arm64.deb'

DEB_NAME = 'libflac8_1.3.2-1_arm64.deb'

SRC_URI[md5sum] = "16e2c08130ec600748e4e0f8a049a731"
