inherit deb-dl

LICENSE = "GPL-2.0+"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/universe/a/a52dec/liba52-0.7.4_0.7.4-19_arm64.deb'

DEB_NAME = 'liba52-0.7.4_0.7.4-19_arm64.deb'

SRC_URI[md5sum] = "c3c8be40ee8e70d942003da55aae1671"
