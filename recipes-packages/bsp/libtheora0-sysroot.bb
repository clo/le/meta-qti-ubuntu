inherit deb-dl

LICENSE = "BSD-3-Clause"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/libt/libtheora/libtheora0_1.1.1+dfsg.1-14_arm64.deb'

DEB_NAME = 'libtheora0_1.1.1+dfsg.1-14_arm64.deb'

SRC_URI[md5sum] = "2787d04901b1f67dc39c1713fd5ddce8"
