inherit deb-dl

LICENSE = "BSD-4-Clause & GPL-3.0+"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/c/cyrus-sasl2/libsasl2-2_2.1.27~101-g0780600+dfsg-3ubuntu2_arm64.deb'

DEB_NAME = 'libsasl2-2_2.1.27~101-g0780600+dfsg-3ubuntu2_arm64.deb'

SRC_URI[md5sum] = "340a402562f718cdfa3b0fb001cabd64"
