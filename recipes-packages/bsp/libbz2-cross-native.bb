inherit native deb-dl

LICENSE = "GPL-2.0 & bzip2-1.0.6"

SRC_URI[md5sum] = "2a9c5b4968a823cd5232606ca35c9cfe"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/b/bzip2/libbz2-1.0_1.0.6-8.1_arm64.deb'
DEB_NAME = 'libbz2-1.0_1.0.6-8.1_arm64.deb'
