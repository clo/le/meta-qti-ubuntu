inherit deb-dl

LICENSE = "MIT"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/p/pixman/libpixman-1-dev_0.34.0-2_arm64.deb'

DEB_NAME = 'libpixman-1-dev_0.34.0-2_arm64.deb'

SRC_URI[md5sum] = "9fc66120d318831c6f62dd17cc5897f3"
