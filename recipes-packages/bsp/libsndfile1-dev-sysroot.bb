inherit deb-dl

LICENSE = "LGPL-2.1"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/libs/libsndfile/libsndfile1-dev_1.0.28-4_arm64.deb'

DEB_NAME = 'libsndfile1-dev_1.0.28-4_arm64.deb'

SRC_URI[md5sum] = "cb9f8158156d89c73cc82f0884ffc0e5"
