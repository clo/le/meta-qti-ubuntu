inherit native deb-dl

LICENSE = "BSD-3-clause"

SRC_URI[md5sum] = "0de5c7e9dc91927f3208c185177ae55f"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/u/util-linux/libuuid1_2.31.1-0.4ubuntu3_arm64.deb'
DEB_NAME = 'libuuid1_2.31.1-0.4ubuntu3_arm64.deb'
