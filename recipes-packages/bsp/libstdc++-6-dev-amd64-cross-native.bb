inherit native deb-dl

LICENSE = "EPL-1.0"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/universe/g/gcc-6-cross/libstdc++-6-dev-amd64-cross_6.4.0-17ubuntu1cross1_all.deb'
DEB_NAME = 'libstdc++-6-dev-amd64-cross_6.4.0-17ubuntu1cross1_all.deb'
SRC_URI[md5sum] = "1c9328ba5748ba59d3004f626f15d31f"
