inherit deb-dl

LICENSE = "BSD-3-clause & GPL-2.0+"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/h/heimdal/libheimntlm0-heimdal_7.5.0+dfsg-1_arm64.deb'

DEB_NAME = 'libheimntlm0-heimdal_7.5.0+dfsg-1_arm64.deb'

SRC_URI[md5sum] = "915b6aefed2fc6759150f57bdb1d8cd0"
