inherit deb-dl

LICENSE = "MIT & GPL-2.0"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/libi/libinput/libinput10_1.10.4-1_arm64.deb'

DEB_NAME = 'libinput10_1.10.4-1_arm64.deb'

SRC_URI[md5sum] = "82299de8d9b603f2f2bc624d4769602b"
