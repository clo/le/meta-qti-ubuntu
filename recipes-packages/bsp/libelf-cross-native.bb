inherit native deb-dl

LICENSE = "(LGPL-3.0+|GPL-2.0) & GPL-3.0"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/e/elfutils/libelf-dev_0.170-0.4_arm64.deb'
DEB_NAME = 'libelf-dev_0.170-0.4_arm64.deb'
SRC_URI[md5sum] = "c1605e611e93ca7de1a85f19e9902f92"
