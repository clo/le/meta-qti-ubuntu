inherit deb-dl

LICENSE = "BSD-3-Clause"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/s/speex/libspeexdsp-dev_1.2~rc1.2-1ubuntu2_arm64.deb'

DEB_NAME = 'libspeexdsp-dev_1.2~rc1.2-1ubuntu2_arm64.deb'

SRC_URI[md5sum] = "a9b4bb2871a366a79a7b09c303634611"
