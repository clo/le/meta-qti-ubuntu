inherit native deb-dl

LICENSE = "MIT & BSD-3-Clause"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/n/ncurses/libtinfo5_6.1-1ubuntu1_arm64.deb'
DEB_NAME = 'libtinfo5_6.1-1ubuntu1_arm64.deb'
SRC_URI[md5sum] = "5dd1b25647dc61e36f7a1307c210e182"
