inherit deb-dl

LICENSE = "MIT & GPL-2.0"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/libi/libinput/libinput-dev_1.10.4-1_arm64.deb'

DEB_NAME = 'libinput-dev_1.10.4-1_arm64.deb'

SRC_URI[md5sum] = "234b91e40a4d6c43ed05b60c2299b44d"
