inherit native deb-dl

LICENSE = "EPL-1.0"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/g/gcc-7-cross/libgcc-7-dev-arm64-cross_7.5.0-3ubuntu1~18.04cross1_all.deb'
DEB_NAME = 'libgcc-7-dev-arm64-cross_7.5.0-3ubuntu1~18.04cross1_all.deb'
SRC_URI[md5sum] = "b9fa8a075462c84c40320d16cde756b9"
