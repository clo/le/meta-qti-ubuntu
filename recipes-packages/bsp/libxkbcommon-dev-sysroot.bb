inherit deb-dl

LICENSE = "MIT & MIT-style"

SRC_URI[md5sum] = "66daaf6b9824939532035e271e6a2820"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/libx/libxkbcommon/libxkbcommon-dev_0.8.0-1ubuntu0.1_arm64.deb'
DEB_NAME = 'libxkbcommon-dev_0.8.0-1ubuntu0.1_arm64.deb'
