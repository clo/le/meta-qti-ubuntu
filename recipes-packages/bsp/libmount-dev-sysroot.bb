inherit native deb-dl

LICENSE = "LGPL-2.1+ & LGPL-3.0"

SRC_URI[md5sum] = "64f680dae3ca0434a014309c1a60983a"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/u/util-linux/libmount-dev_2.31.1-0.4ubuntu3_arm64.deb'
DEB_NAME = 'libmount-dev_2.31.1-0.4ubuntu3_arm64.deb'
