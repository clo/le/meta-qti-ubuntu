inherit deb-dl

LICENSE = "Qhull & GPL-3.0+"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/universe/q/qhull/libqhull-dev_2015.2-4_arm64.deb'

DEB_NAME = 'libqhull-dev_2015.2-4_arm64.deb'

SRC_URI[md5sum] = "86ec54c06cf9121e2d9e205aafc1672e"
