inherit native deb-dl

LICENSE = "LGPL-2.1"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/libn/libnl3/libnl-genl-3-dev_3.2.29-0ubuntu3_arm64.deb'
DEB_NAME = 'libnl-genl-3-dev_3.2.29-0ubuntu3_arm64.deb'
SRC_URI[md5sum] = "628b3889a86875640f6c59ba857704c9"