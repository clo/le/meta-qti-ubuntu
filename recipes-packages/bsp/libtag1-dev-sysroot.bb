inherit deb-dl

LICENSE = "(LGPL-2.1 | MPL-1.1) & BSD-2-Clause"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/t/taglib/libtag1-dev_1.11.1+dfsg.1-0.2build2_arm64.deb'

DEB_NAME = 'libtag1-dev_1.11.1+dfsg.1-0.2build2_arm64.deb'

SRC_URI[md5sum] = "e619e8e0e8f7a31f18f1b58953a76756"
