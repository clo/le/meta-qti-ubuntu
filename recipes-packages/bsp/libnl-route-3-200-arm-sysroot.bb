inherit native deb-dl

LICENSE = "LGPL-2.1"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/libn/libnl3/libnl-route-3-200_3.2.29-0ubuntu3_arm64.deb'
DEB_NAME = 'libnl-route-3-200_3.2.29-0ubuntu3_arm64.deb'
SRC_URI[md5sum] = "fb4d37bf9b82ea641cece8fc6beda144"
