inherit deb-dl

LICENSE = "GPLv2 & BSD-2-Clause"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/l/lz4/liblz4-dev_0.0~r131-2ubuntu3_arm64.deb'
DEB_NAME = 'liblz4-dev_0.0~r131-2ubuntu3_arm64.deb'

SRC_URI[md5sum] = "9115f4db1ddc33e7b4bfa0a832a21add"
