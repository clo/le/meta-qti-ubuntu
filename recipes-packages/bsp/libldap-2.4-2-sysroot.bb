inherit deb-dl

LICENSE = "OpenLDAP"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/o/openldap/libldap-2.4-2_2.4.45+dfsg-1ubuntu1_arm64.deb'

DEB_NAME = 'libldap-2.4-2_2.4.45+dfsg-1ubuntu1_arm64.deb'

SRC_URI[md5sum] = "90531a6601fb8a7e6284cbcd2b234491"
