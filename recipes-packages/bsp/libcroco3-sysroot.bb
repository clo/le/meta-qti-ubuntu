inherit deb-dl

LICENSE = " LGPL-2.0+"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/libc/libcroco/libcroco3_0.6.12-2_arm64.deb'

DEB_NAME = 'libcroco3_0.6.12-2_arm64.deb'

SRC_URI[md5sum] = "c761bfcc06740680f6b52b6842be7667"
