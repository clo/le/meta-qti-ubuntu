inherit deb-dl

LICENSE = "BSD-3-clause & GPL-2.0+"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/h/heimdal/libgssapi3-heimdal_7.5.0+dfsg-1_arm64.deb'

DEB_NAME = 'libgssapi3-heimdal_7.5.0+dfsg-1_arm64.deb'

SRC_URI[md5sum] = "c5c5ccedf69c8593e4d983ca1bf1bfce"
