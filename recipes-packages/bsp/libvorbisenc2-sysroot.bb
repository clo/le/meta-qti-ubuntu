inherit deb-dl

LICENSE = "BSD-3-Clause"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/libv/libvorbis/libvorbisenc2_1.3.5-4.2_arm64.deb'

DEB_NAME = 'libvorbisenc2_1.3.5-4.2_arm64.deb'

SRC_URI[md5sum] = "3f24b337f691cc9304367eb29392e4e2"
