inherit deb-dl

LICENSE = "(LGPL-2.1 | MPL-1.1) & BSD-2-Claus"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/t/taglib/libtag1v5-vanilla_1.11.1+dfsg.1-0.2build2_arm64.deb'

DEB_NAME = 'libtag1v5-vanilla_1.11.1+dfsg.1-0.2build2_arm64.deb'

SRC_URI[md5sum] = "770a2e741dba46e623e7bb942339932b"
