inherit deb-dl

LICENSE = "MIT"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/m/mtdev/libmtdev1_1.1.5-1ubuntu3_arm64.deb'

DEB_NAME = 'libmtdev1_1.1.5-1ubuntu3_arm64.deb'

SRC_URI[md5sum] = "1aee80c219170dd53b24f11f0875becb"
