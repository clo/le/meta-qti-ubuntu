inherit deb-dl

LICENSE = "GPL-2.0+ | LGPL-3.0+"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/g/gmp/libgmp10_6.1.2+dfsg-2_arm64.deb'

DEB_NAME = 'libgmp10_6.1.2+dfsg-2_arm64.deb'

SRC_URI[md5sum] = "298bb74509b1c6dccf516c006daca258"
