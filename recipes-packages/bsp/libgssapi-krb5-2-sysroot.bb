inherit deb-dl

LICENSE = "MIT & BSD-3-Clause & more"
#For complete LICENSE info refer to this file http://changelogs.ubuntu.com/changelogs/pool/main/k/krb5/krb5_1.16-2ubuntu0.1/copyright

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/k/krb5/libgssapi-krb5-2_1.16-2build1_arm64.deb'

DEB_NAME = 'libgssapi-krb5-2_1.16-2build1_arm64.deb'

SRC_URI[md5sum] = "fea5c6e592ceed49889219e903ddce78"
