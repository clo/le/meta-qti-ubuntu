inherit deb-dl

LICENSE = "X11 & GPL-2.0+"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/libe/libevdev/libevdev2_1.5.8+dfsg-1_arm64.deb'

DEB_NAME = 'libevdev2_1.5.8+dfsg-1_arm64.deb'

SRC_URI[md5sum] = "e3857c75cbb595b6fce39fbcb380093a"
