inherit deb-dl

LICENSE = "BSD-3-Clause"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/s/speex/libspeex1_1.2~rc1.2-1ubuntu2_arm64.deb'

DEB_NAME = 'libspeex1_1.2~rc1.2-1ubuntu2_arm64.deb'

SRC_URI[md5sum] = "1621f5d386b2b990a8d4d01ccb85c2f3"
