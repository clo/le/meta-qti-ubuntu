inherit deb-dl

LICENSE = "LGPL-2.0+ & GPL-2.0+"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/libr/librsvg/librsvg2-dev_2.40.20-2_arm64.deb'

DEB_NAME = 'librsvg2-dev_2.40.20-2_arm64.deb'

SRC_URI[md5sum] = "9d8a7eea57b643812cf88a307bdd6313"
