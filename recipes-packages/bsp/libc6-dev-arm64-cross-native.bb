inherit native deb-dl

LICENSE = "EPL-1.0"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/c/cross-toolchain-base/libc6-dev-arm64-cross_2.27-3ubuntu1cross1_all.deb'
DEB_NAME = 'libc6-dev-arm64-cross_2.27-3ubuntu1cross1_all.deb'
SRC_URI[md5sum] = "e1c372df6256fc28abd04b49330c7e7f"
