inherit deb-dl

LICENSE = "BSD-3-Clause | GPL-2.0+"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/libc/libcap2/libcap-dev_2.25-1.2_arm64.deb'

DEB_NAME = 'libcap-dev_2.25-1.2_arm64.deb'

SRC_URI[md5sum] = "d4bc837f069cb9dfedbe9fdac9d3eaff"
