inherit native deb-dl

LICENSE = "BSD-3-Clause"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/p/pcre3/libpcre3-dev_8.39-9_arm64.deb'
DEB_NAME = 'libpcre3-dev_8.39-9_arm64.deb'
SRC_URI[md5sum] = "0d18c770371445047ba05ab29a63ca6a"
