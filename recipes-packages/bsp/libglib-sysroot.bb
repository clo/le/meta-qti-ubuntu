inherit native deb-dl

LICENSE = "LGPL-2.0+"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/g/glib2.0/libglib2.0-0_2.56.1-2ubuntu1_arm64.deb'
DEB_NAME = 'libglib2.0-0_2.56.1-2ubuntu1_arm64.deb'
SRC_URI[md5sum] = "1ba37b7d8d9de50d615ac46efc76dc19"
