inherit deb-dl

LICENSE = "MIT-style"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/libx/libxcb/libxcb-shm0_1.13-1_arm64.deb'
DEB_NAME = 'libxcb-shm0_1.13-1_arm64.deb'

SRC_URI[md5sum] = "d3ecc64ca3d9f07ff3e3d4114cfdc8d5"
