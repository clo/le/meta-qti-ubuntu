inherit deb-dl

LICENSE = "MIT-style"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/libx/libxcb/libxcb-render0_1.13-1_arm64.deb'
DEB_NAME = 'libxcb-render0_1.13-1_arm64.deb'

SRC_URI[md5sum] = "d865b94b77c99b27cbd224526397f91d"
