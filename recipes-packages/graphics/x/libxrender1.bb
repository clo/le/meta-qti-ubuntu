inherit deb-dl

LICENSE = "MIT-style"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/libx/libxrender/libxrender1_0.9.10-1_arm64.deb'
DEB_NAME = 'libxrender1_0.9.10-1_arm64.deb'

SRC_URI[md5sum] = "bc735af99800cf2014aa58b2ec4c1c44"
