inherit deb-dl

LICENSE = "MIT-style"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/libx/libxau/libxau-dev_1.0.8-1_arm64.deb'
DEB_NAME = 'libxau-dev_1.0.8-1_arm64.deb'

SRC_URI[md5sum] = "9008506ed58496228b5e861f38f4abcf"
