inherit deb-dl

LICENSE = "MIT-style"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/libx/libxau/libxau6_1.0.8-1_arm64.deb'
DEB_NAME = 'libxau6_1.0.8-1_arm64.deb'

SRC_URI[md5sum] = "cfbe787da66cb65a539f23ee5cefe1c7"
