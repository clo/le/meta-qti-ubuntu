inherit deb-dl

LICENSE = "MIT-style"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/libx/libxcb/libxcb-render0-dev_1.13-1_arm64.deb'
DEB_NAME = 'libxcb-render0-dev_1.13-1_arm64.deb'

SRC_URI[md5sum] = "7636f0f373af90b5333ddcec50ed1e10"
