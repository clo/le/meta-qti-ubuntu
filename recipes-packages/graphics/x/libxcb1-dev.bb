inherit deb-dl

LICENSE = "MIT-style"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/libx/libxcb/libxcb1-dev_1.13-1_arm64.deb'
DEB_NAME = 'libxcb1-dev_1.13-1_arm64.deb'

SRC_URI[md5sum] = "33bf92868bd664ebb886218f7ec64b2f"
