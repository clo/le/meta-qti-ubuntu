inherit deb-dl

LICENSE = "MIT-style"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/libx/libxrender/libxrender-dev_0.9.10-1_arm64.deb'
DEB_NAME = 'libxrender-dev_0.9.10-1_arm64.deb'

SRC_URI[md5sum] = "2d09206560e001bcb9b455ea92504bb8"
