inherit deb-dl

LICENSE = "MIT-style"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/libx/libxdmcp/libxdmcp6_1.1.2-3_arm64.deb'
DEB_NAME = 'libxdmcp6_1.1.2-3_arm64.deb'

SRC_URI[md5sum] = "0dc7722834da7bb9cf5a826647a09b4d"
