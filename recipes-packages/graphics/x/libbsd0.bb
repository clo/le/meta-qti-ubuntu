inherit deb-dl

LICENSE = "BSD & PD & ISC & MIT"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/libb/libbsd/libbsd0_0.8.7-1_arm64.deb'
DEB_NAME = 'libbsd0_0.8.7-1_arm64.deb'

SRC_URI[md5sum] = "f3485e502247628670b071119f488c49"
