inherit deb-dl

LICENSE = "MIT-style"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/libx/libxext/libxext-dev_1.3.3-1_arm64.deb'
DEB_NAME = 'libxext-dev_1.3.3-1_arm64.deb'

SRC_URI[md5sum] = "fe9335a5d5fa5de5372d75f3b813d835"
