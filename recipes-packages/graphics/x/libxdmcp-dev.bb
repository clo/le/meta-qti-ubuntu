inherit deb-dl

LICENSE = "MIT-style"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/libx/libxdmcp/libxdmcp-dev_1.1.2-3_arm64.deb'
DEB_NAME = 'libxdmcp-dev_1.1.2-3_arm64.deb'

SRC_URI[md5sum] = "7fe9ac588a0f20ecbc2c55f68f5af71b"
