inherit deb-dl

LICENSE = "MIT-style"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/libx/libxext/libxext6_1.3.3-1_arm64.deb'
DEB_NAME = 'libxext6_1.3.3-1_arm64.deb'

SRC_URI[md5sum] = "bfc5612ddda70258ea9372076d4f1c1d"
