inherit deb-dl

LICENSE = "LGPL-2.1 | MPL-1.1"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/c/cairo/libcairo2-dev_1.15.10-2_arm64.deb'
DEB_NAME = 'libcairo2-dev_1.15.10-2_arm64.deb'

SRC_URI[md5sum] = "2e78f7eedc052e02f3e30ba91b615f5c"
