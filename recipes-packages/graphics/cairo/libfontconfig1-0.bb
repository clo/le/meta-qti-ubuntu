inherit deb-dl

LICENSE = "MIT-style"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/f/fontconfig/libfontconfig1_2.12.6-0ubuntu2_arm64.deb'
DEB_NAME = 'libfontconfig1_2.12.6-0ubuntu2_arm64.deb'

SRC_URI[md5sum] = "b805eb055b7333f000bd085899b89ae9"
