inherit deb-dl

LICENSE = "MIT-style"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/f/fontconfig/libfontconfig1-dev_2.12.6-0ubuntu2_arm64.deb'
DEB_NAME = 'libfontconfig1-dev_2.12.6-0ubuntu2_arm64.deb'

SRC_URI[md5sum] = "19ce6b4f7350649efd1243006586802e"
