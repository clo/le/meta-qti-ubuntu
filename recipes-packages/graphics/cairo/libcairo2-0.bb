inherit deb-dl

LICENSE = "LGPL-2.1 | MPL-1.1"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/c/cairo/libcairo2_1.15.10-2_arm64.deb'
DEB_NAME = 'libcairo2_1.15.10-2_arm64.deb'

SRC_URI[md5sum] = "3a15a3c022efc4735289caacc26e265c"
