inherit deb-dl

LICENSE = "FTL | GPL-2.0+"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/f/freetype/libfreetype6_2.8.1-2ubuntu2_arm64.deb'
DEB_NAME = 'libfreetype6_2.8.1-2ubuntu2_arm64.deb'

SRC_URI[md5sum] = "d865b94b77c99b27cbd224526397f91d"
