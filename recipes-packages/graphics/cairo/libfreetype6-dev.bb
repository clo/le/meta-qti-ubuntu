inherit deb-dl

LICENSE = "FTL | GPL-2.0+"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/f/freetype/libfreetype6-dev_2.8.1-2ubuntu2_arm64.deb'
DEB_NAME = 'libfreetype6-dev_2.8.1-2ubuntu2_arm64.deb'

SRC_URI[md5sum] = "77217ad85a05a67375a2f3ed806fc128"
