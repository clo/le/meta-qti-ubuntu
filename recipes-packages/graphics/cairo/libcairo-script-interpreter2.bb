inherit deb-dl

LICENSE = "LGPL-2.1 | MPL-1.1"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/c/cairo/libcairo-script-interpreter2_1.15.10-2_arm64.deb'
DEB_NAME = 'libcairo-script-interpreter2_1.15.10-2_arm64.deb'

SRC_URI[md5sum] = "cdd3a9e0a3d01ec7bdc81f9b8e57d523"
