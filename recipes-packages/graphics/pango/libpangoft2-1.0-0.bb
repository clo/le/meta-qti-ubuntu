inherit deb-dl

LICENSE = "LGPL-2.0+"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/p/pango1.0/libpangoft2-1.0-0_1.40.14-1_arm64.deb'
DEB_NAME = 'libpangoft2-1.0-0_1.40.14-1_arm64.deb'

SRC_URI[md5sum] = "e3a26e1fc056ea78071c13182e6ebf10"
