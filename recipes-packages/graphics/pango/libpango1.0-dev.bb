inherit deb-dl

LICENSE = "LGPL-2.0+"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/p/pango1.0/libpango1.0-dev_1.40.14-1_arm64.deb'
DEB_NAME = 'libpango1.0-dev_1.40.14-1_arm64.deb'

SRC_URI[md5sum] = "27b239b21e82a8d59383e91c0cc6ca99"
