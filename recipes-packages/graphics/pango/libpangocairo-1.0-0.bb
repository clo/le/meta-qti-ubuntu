inherit deb-dl

LICENSE = "LGPL-2.0+"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/p/pango1.0/libpangocairo-1.0-0_1.40.14-1_arm64.deb'
DEB_NAME = 'libpangocairo-1.0-0_1.40.14-1_arm64.deb'

SRC_URI[md5sum] = "5067d5b8708f54f196ac5b9b8b019b7a"
