inherit deb-dl

LICENSE = "LGPL-2.0+"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/p/pango1.0/gir1.2-pango-1.0_1.40.14-1_arm64.deb'
DEB_NAME = 'gir1.2-pango-1.0_1.40.14-1_arm64.deb'

SRC_URI[md5sum] = "9dc206ee223663c138683f71a6685dee"
