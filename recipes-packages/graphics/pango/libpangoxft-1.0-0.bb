inherit deb-dl

LICENSE = "LGPL-2.0+"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/p/pango1.0/libpangoxft-1.0-0_1.40.14-1_arm64.deb'
DEB_NAME = 'libpangoxft-1.0-0_1.40.14-1_arm64.deb'

SRC_URI[md5sum] = "3e780b7e52e554c4ca322b58c34a0c66"
