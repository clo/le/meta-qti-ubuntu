inherit deb-dl

LICENSE = "LGPL-2.0+"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/p/pango1.0/libpango-1.0-0_1.40.14-1_arm64.deb'
DEB_NAME = 'libpango-1.0-0_1.40.14-1_arm64.deb'

SRC_URI[md5sum] = "40201f1d5747ac64658ecc3eb8185d61"
