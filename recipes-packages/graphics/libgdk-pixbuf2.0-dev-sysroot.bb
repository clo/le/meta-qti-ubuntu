inherit deb-dl

LICENSE = "LGPL-2.0"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/g/gdk-pixbuf/libgdk-pixbuf2.0-dev_2.36.11-2_arm64.deb'
DEB_NAME = 'libgdk-pixbuf2.0-dev_2.36.11-2_arm64.deb'

SRC_URI[md5sum] = "76d91032c4a20c31c37966526cfeb4e2"
