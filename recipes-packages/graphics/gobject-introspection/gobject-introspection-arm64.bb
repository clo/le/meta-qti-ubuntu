inherit deb-dl

LICENSE = "LGPL-2.0+ & GPL-2.0+ & MIT"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/g/gobject-introspection/gobject-introspection_1.56.1-1_arm64.deb'
DEB_NAME = 'gobject-introspection_1.56.1-1_arm64.deb'

SRC_URI[md5sum] = "3846a2b50801625c3075fb5474396200"
