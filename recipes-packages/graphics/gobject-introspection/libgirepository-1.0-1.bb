inherit deb-dl

LICENSE = "LGPL-2.0+ & GPL-2.0+ & MIT"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/g/gobject-introspection/libgirepository-1.0-1_1.56.1-1_arm64.deb'
DEB_NAME = 'libgirepository-1.0-1_1.56.1-1_arm64.deb'

SRC_URI[md5sum] = "5be2a793d80dd270719f065c30ebeef3"
