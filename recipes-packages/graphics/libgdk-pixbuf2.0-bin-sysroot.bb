inherit deb-dl

LICENSE = "LGPL-2.0+ & GPL-2.0+"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/g/gdk-pixbuf/libgdk-pixbuf2.0-bin_2.36.11-2_arm64.deb'
DEB_NAME = 'libgdk-pixbuf2.0-bin_2.36.11-2_arm64.deb'

SRC_URI[md5sum] = "adfa6b08027443e9b8333e51a4631386"
