LICENSE = "Apache-2.0"
SRC_URI[md5sum] = "7608a2450666c481b518d8250ccc8872"

FULL_LINK = 'http://packages.ros.org/ros2/ubuntu/pool/main/r/ros-dashing-std-msgs/ros-dashing-std-msgs_0.7.1-1bionic.20210521.221441_arm64.deb'
DEB_NAME = 'ros-dashing-std-msgs_0.7.1-1bionic.20210521.221441_arm64.deb'
