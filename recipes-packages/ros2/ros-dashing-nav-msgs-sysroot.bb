LICENSE = "Apache-2.0"

FULL_LINK = 'http://packages.ros.org/ros2/ubuntu/pool/main/r/ros-dashing-nav-msgs/ros-dashing-nav-msgs_0.7.1-1bionic.20210521.224407_arm64.deb'
DEB_NAME = 'ros-dashing-nav-msgs_0.7.1-1bionic.20210521.224407_arm64.deb'

SRC_URI[md5sum] = "60383ff2f566a4a0a491c27464fed5f0"
