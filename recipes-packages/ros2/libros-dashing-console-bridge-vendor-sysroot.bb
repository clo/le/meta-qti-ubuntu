LICENSE = "Apache-2.0 & BSD"
SRC_URI[md5sum] = "6d0e7085f5dc4536283a4312ad13e4ef"

FULL_LINK = 'http://packages.ros.org/ros2/ubuntu/pool/main/r/ros-dashing-console-bridge-vendor/ros-dashing-console-bridge-vendor_1.2.0-1bionic.20201125.040006_arm64.deb'
DEB_NAME = 'ros-dashing-console-bridge-vendor_1.2.0-1bionic.20201125.040006_arm64.deb'
