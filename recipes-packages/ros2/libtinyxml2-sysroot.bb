inherit native deb-dl

LICENSE = "Zlib & GPL-2.0+"
SRC_URI[md5sum] = "dd2d1ef6c6ee18306c6a08ad8b6d75a1"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/universe/t/tinyxml2/libtinyxml2-6_6.0.0+dfsg-1_arm64.deb'
DEB_NAME = 'libtinyxml2-6_6.0.0+dfsg-1_arm64.deb'
