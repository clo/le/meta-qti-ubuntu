LICENSE = "Apache-2.0"

SRC_URI[md5sum] = "a8efabb3c8e6a6ea92e8adcc7d22f090"

FULL_LINK = 'http://packages.ros.org/ros2/ubuntu/pool/main/r/ros-dashing-rosgraph-msgs/ros-dashing-rosgraph-msgs_0.7.4-1bionic.20210226.173828_arm64.deb'
DEB_NAME = 'ros-dashing-rosgraph-msgs_0.7.4-1bionic.20210226.173828_arm64.deb'
