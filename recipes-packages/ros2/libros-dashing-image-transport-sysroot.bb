LICENSE = "BSD"
SRC_URI[md5sum] = "634d0e3513267d786fb4dbf18790659d"

FULL_LINK = 'http://packages.ros.org/ros2/ubuntu/pool/main/r/ros-dashing-image-transport/ros-dashing-image-transport_2.1.1-1bionic.20210522.003628_arm64.deb'
DEB_NAME = 'ros-dashing-image-transport_2.1.1-1bionic.20210522.003628_arm64.deb'
