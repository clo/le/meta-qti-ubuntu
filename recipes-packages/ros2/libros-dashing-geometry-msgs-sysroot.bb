LICENSE = "Apache-2.0"

SRC_URI[md5sum] = "b8e339a4518dea507fc6fd3887ebd4fa"
FULL_LINK = 'http://packages.ros.org/ros2/ubuntu/pool/main/r/ros-dashing-geometry-msgs/ros-dashing-geometry-msgs_0.7.1-1bionic.20210521.222927_arm64.deb'
DEB_NAME = 'ros-dashing-geometry-msgs_0.7.1-1bionic.20210521.222927_arm64.deb'
