LICENSE = "Apache-2.0"
SRC_URI[md5sum] = "566f1bd5dc49a4c6f9d783b2cdf6561b"

FULL_LINK = 'http://packages.ros.org/ros2/ubuntu/pool/main/r/ros-dashing-rosidl-typesupport-interface/ros-dashing-rosidl-typesupport-interface_0.7.10-1bionic.20201125.050940_arm64.deb'
DEB_NAME = 'ros-dashing-rosidl-typesupport-interface_0.7.10-1bionic.20201125.050940_arm64.deb'
