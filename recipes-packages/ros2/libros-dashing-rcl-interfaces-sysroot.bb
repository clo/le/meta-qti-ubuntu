LICENSE = "Apache-2.0"

SRC_URI[md5sum] = "3d93dbedf0d3299ce6c9e5e7b0b9bc14"

FULL_LINK = 'http://packages.ros.org/ros2/ubuntu/pool/main/r/ros-dashing-rcl-interfaces/ros-dashing-rcl-interfaces_0.7.4-1bionic.20210226.173808_arm64.deb'
DEB_NAME = 'ros-dashing-rcl-interfaces_0.7.4-1bionic.20210226.173808_arm64.deb'
