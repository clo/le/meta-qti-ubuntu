inherit native deb-dl

LICENSE = "BSL-1.0 & BSD-3-clause & MIT & PD & Zlib"
SRC_URI[md5sum] = "55872d854116c6612abc39b068b30b5c"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/universe/p/poco/libpocofoundation50_1.8.0.1-1ubuntu4_arm64.deb'
DEB_NAME = 'libpocofoundation50_1.8.0.1-1ubuntu4_arm64.deb'
