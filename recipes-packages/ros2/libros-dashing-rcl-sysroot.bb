LICENSE = "Apache-2.0"
SRC_URI[md5sum] = "807d024a0029dbc82014d0e9d29961e3"

FULL_LINK = 'http://packages.ros.org/ros2/ubuntu/pool/main/r/ros-dashing-rcl/ros-dashing-rcl_0.7.10-1bionic.20210521.221011_arm64.deb'
DEB_NAME = 'ros-dashing-rcl_0.7.10-1bionic.20210521.221011_arm64.deb'
