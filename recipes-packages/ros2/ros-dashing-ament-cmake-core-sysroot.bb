LICENSE = "Apache-2.0"

FULL_LINK = 'http://packages.ros.org/ros2/ubuntu/pool/main/r/ros-dashing-ament-cmake-core/ros-dashing-ament-cmake-core_0.7.6-1bionic.20201125.032254_arm64.deb'
DEB_NAME = 'ros-dashing-ament-cmake-core_0.7.6-1bionic.20201125.032254_arm64.deb'

SRC_URI[md5sum] = "9824042eda082e66ef0f8e1951543726"
