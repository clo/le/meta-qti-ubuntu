LICENSE = "Apache-2.0"
SRC_URI[md5sum] = "3780e865a66113c6f91a1d379bea016e"

FULL_LINK = 'http://packages.ros.org/ros2/ubuntu/pool/main/r/ros-dashing-rosidl-generator-cpp/ros-dashing-rosidl-generator-cpp_0.7.10-1bionic.20201125.052401_arm64.deb'
DEB_NAME = 'ros-dashing-rosidl-generator-cpp_0.7.10-1bionic.20201125.052401_arm64.deb'
