inherit native deb-dl

LICENSE = "BSL-1.0 & BSD-3-clause & MIT & PD & Zlib"
SRC_URI[md5sum] = "19319a1ca5162a6ecf396a81046081ce"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/universe/p/poco/libpoco-dev_1.8.0.1-1ubuntu4_arm64.deb'
DEB_NAME = 'libpoco-dev_1.8.0.1-1ubuntu4_arm64.deb'
