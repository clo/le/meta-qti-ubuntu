LICENSE = "Apache-2.0"
SRC_URI[md5sum] = "503adefa3d7a2f7f3a78b3d5a1e638be"

FULL_LINK = 'http://packages.ros.org/ros2/ubuntu/pool/main/r/ros-dashing-rcl-yaml-param-parser/ros-dashing-rcl-yaml-param-parser_0.7.10-1bionic.20210521.221531_arm64.deb'
DEB_NAME = 'ros-dashing-rcl-yaml-param-parser_0.7.10-1bionic.20210521.221531_arm64.deb'
