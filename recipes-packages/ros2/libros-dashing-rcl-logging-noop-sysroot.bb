LICENSE = "Apache-2.0"

SRC_URI[md5sum] = "32a1294cba662582d7b1de08cc78686b"

FULL_LINK = 'http://packages.ros.org/ros2/ubuntu/pool/main/r/ros-dashing-rcl-logging-noop/ros-dashing-rcl-logging-noop_0.2.1-1bionic.20201125.052907_arm64.deb'
DEB_NAME = 'ros-dashing-rcl-logging-noop_0.2.1-1bionic.20201125.052907_arm64.deb'
