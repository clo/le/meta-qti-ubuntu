LICENSE = "Apache-2.0"
SRC_URI[md5sum] = "9220ab1a63baa5eca437f10710e4c5d2"

FULL_LINK = 'http://packages.ros.org/ros2/ubuntu/pool/main/r/ros-dashing-rosidl-generator-c/ros-dashing-rosidl-generator-c_0.7.10-1bionic.20201125.051944_arm64.deb'
DEB_NAME = 'ros-dashing-rosidl-generator-c_0.7.10-1bionic.20201125.051944_arm64.deb'
