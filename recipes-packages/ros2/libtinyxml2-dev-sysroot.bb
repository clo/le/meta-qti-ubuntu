inherit native deb-dl

LICENSE = "Zlib & GPL-2.0+"
SRC_URI[md5sum] = "ab9af1b705e5baf4fb52335ffb892a53"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/universe/t/tinyxml2/libtinyxml2-dev_6.0.0+dfsg-1_arm64.deb'
DEB_NAME = 'libtinyxml2-dev_6.0.0+dfsg-1_arm64.deb'
