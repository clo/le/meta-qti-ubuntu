LICENSE = "BSD"
SRC_URI[md5sum] = "d3de82c2d1a53e2eb582b8bf712d50dd"

FULL_LINK = 'http://packages.ros.org/ros2/ubuntu/pool/main/r/ros-dashing-message-filters/ros-dashing-message-filters_3.1.3-1bionic.20210521.230805_arm64.deb'
DEB_NAME = 'ros-dashing-message-filters_3.1.3-1bionic.20210521.230805_arm64.deb'
