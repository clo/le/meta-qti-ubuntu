LICENSE = "BSD"
SRC_URI[md5sum] = "ea39a4517977cbaff4f6a6c4c8856dc4"

FULL_LINK = 'http://packages.ros.org/ros2/ubuntu/pool/main/r/ros-dashing-class-loader/ros-dashing-class-loader_1.3.3-1bionic.20201125.054417_arm64.deb'
DEB_NAME = 'ros-dashing-class-loader_1.3.3-1bionic.20201125.054417_arm64.deb'
