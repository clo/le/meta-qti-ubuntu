LICENSE = "Apache-2.0"
SRC_URI[md5sum] = "7fd599bacfdf1e93f8666546772adda8"

FULL_LINK = 'http://packages.ros.org/ros2/ubuntu/pool/main/r/ros-dashing-rcutils/ros-dashing-rcutils_0.7.6-1bionic.20201125.051528_arm64.deb'
DEB_NAME = 'ros-dashing-rcutils_0.7.6-1bionic.20201125.051528_arm64.deb'
