LICENSE = "Apache-2.0"
SRC_URI[md5sum] = "47f52d19a9788d3eadfbf07d73425297"

FULL_LINK = 'http://packages.ros.org/ros2/ubuntu/pool/main/r/ros-dashing-rmw-implementation/ros-dashing-rmw-implementation_0.7.2-1bionic.20210226.170621_arm64.deb'
DEB_NAME = 'ros-dashing-rmw-implementation_0.7.2-1bionic.20210226.170621_arm64.deb'
