LICENSE = "Apache-2.0"
SRC_URI[md5sum] = "fee74e843c90c7bb7835a39d3cce0587"

FULL_LINK = 'http://packages.ros.org/ros2/ubuntu/pool/main/r/ros-dashing-rmw/ros-dashing-rmw_0.7.2-1bionic.20201125.053247_arm64.deb'
DEB_NAME = 'ros-dashing-rmw_0.7.2-1bionic.20201125.053247_arm64.deb'
