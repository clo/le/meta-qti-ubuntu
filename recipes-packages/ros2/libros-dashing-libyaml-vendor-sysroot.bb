LICENSE = "MIT & Apache-2.0"

SRC_URI[md5sum] = "3dabedf2d84773833f60402489bc96e3"

FULL_LINK = 'http://packages.ros.org/ros2/ubuntu/pool/main/r/ros-dashing-libyaml-vendor/ros-dashing-libyaml-vendor_1.0.0-1bionic.20201125.041225_arm64.deb'
DEB_NAME = 'ros-dashing-libyaml-vendor_1.0.0-1bionic.20201125.041225_arm64.deb'
