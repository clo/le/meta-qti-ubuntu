LICENSE = "Apache-2.0"
SRC_URI[md5sum] = "e4a6d4b2a93f1aa7100b4a29b49d96ce"

FULL_LINK = 'http://packages.ros.org/ros2/ubuntu/pool/main/r/ros-dashing-sensor-msgs/ros-dashing-sensor-msgs_0.7.1-1bionic.20210521.224423_arm64.deb'
DEB_NAME = 'ros-dashing-sensor-msgs_0.7.1-1bionic.20210521.224423_arm64.deb'
