LICENSE = "Apache-2.0"
SRC_URI[md5sum] = "89dbba2d817be051cccfdc93d3d76aed"

FULL_LINK = 'http://packages.ros.org/ros2/ubuntu/pool/main/r/ros-dashing-rclcpp/ros-dashing-rclcpp_0.7.16-1bionic.20210521.221938_arm64.deb'
DEB_NAME = 'ros-dashing-rclcpp_0.7.16-1bionic.20210521.221938_arm64.deb'
