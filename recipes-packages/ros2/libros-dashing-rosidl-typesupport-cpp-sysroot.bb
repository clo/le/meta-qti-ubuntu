LICENSE = "Apache-2.0"
SRC_URI[md5sum] = "3132e9f8a6d7f026cde0951d483695fc"

FULL_LINK = 'http://packages.ros.org/ros2/ubuntu/pool/main/r/ros-dashing-rosidl-typesupport-cpp/ros-dashing-rosidl-typesupport-cpp_0.7.1-1bionic.20210226.171645_arm64.deb'
DEB_NAME = 'ros-dashing-rosidl-typesupport-cpp_0.7.1-1bionic.20210226.171645_arm64.deb'
