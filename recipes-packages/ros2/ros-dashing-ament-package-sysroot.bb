
LICENSE = "Apache-2.0"

FULL_LINK = 'http://packages.ros.org/ros2/ubuntu/pool/main/r/ros-dashing-ament-package/ros-dashing-ament-package_0.7.3-1bionic.20191011.215328_arm64.deb'
DEB_NAME = 'ros-dashing-ament-package_0.7.3-1bionic.20191011.215328_arm64.deb'

SRC_URI[md5sum] = "2caf4d15cd31c7a23aed58470cb4de99"
