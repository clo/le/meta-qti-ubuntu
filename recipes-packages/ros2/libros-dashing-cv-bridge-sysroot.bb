LICENSE = "BSD"
SRC_URI[md5sum] = "4855c1b7a626ebfd42f76ab1f9640eff"

FULL_LINK = 'http://packages.ros.org/ros2/ubuntu/pool/main/r/ros-dashing-cv-bridge/ros-dashing-cv-bridge_2.1.4-1bionic.20210522.014831_arm64.deb'
DEB_NAME = 'ros-dashing-cv-bridge_2.1.4-1bionic.20210522.014831_arm64.deb'
