LICENSE = "Apache-2.0"
SRC_URI[md5sum] = "a5cef4a1296c38829e85e4ecdef95e9e"

FULL_LINK = 'http://packages.ros.org/ros2/ubuntu/pool/main/r/ros-dashing-rosidl-typesupport-c/ros-dashing-rosidl-typesupport-c_0.7.1-1bionic.20210226.170952_arm64.deb'
DEB_NAME = 'ros-dashing-rosidl-typesupport-c_0.7.1-1bionic.20210226.170952_arm64.deb'
