LICENSE = "Apache-2.0"
SRC_URI[md5sum] = "ed100d3f26db4a21582e1878c9e0b59a"

FULL_LINK = 'http://packages.ros.org/ros2/ubuntu/pool/main/r/ros-dashing-ament-index-cpp/ros-dashing-ament-index-cpp_0.7.2-1bionic.20201125.052249_arm64.deb'
DEB_NAME = 'ros-dashing-ament-index-cpp_0.7.2-1bionic.20201125.052249_arm64.deb'
