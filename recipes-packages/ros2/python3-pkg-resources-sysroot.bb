
LICENSE = "MIT"

FULL_LINK = 'http://ports.ubuntu.com/ubuntu-ports/pool/main/p/python-setuptools/python3-pkg-resources_39.0.1-2_all.deb'
DEB_NAME = 'python3-pkg-resources_39.0.1-2_all.deb'

SRC_URI[md5sum] = "4369a00fddeb98cbad71804feee824c6"
