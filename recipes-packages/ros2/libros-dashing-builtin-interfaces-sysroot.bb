LICENSE = "Apache-2.0"

SRC_URI[md5sum] = "4d17757cb991f502c178dd23d9ddeb6a"

FULL_LINK = 'http://packages.ros.org/ros2/ubuntu/pool/main/r/ros-dashing-builtin-interfaces/ros-dashing-builtin-interfaces_0.7.4-1bionic.20210226.172819_arm64.deb'
DEB_NAME = 'ros-dashing-builtin-interfaces_0.7.4-1bionic.20210226.172819_arm64.deb'
