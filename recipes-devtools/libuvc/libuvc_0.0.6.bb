SUMMARY = "A cross-platform library for USB video devices"
HOMEPAGE = "https://github.com/libuvc/libuvc"
SECTION = "libs"

LICENSE = "BSD-3-Clause"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/files/common-licenses/\
${LICENSE};md5=550794465ba0ec5312d6919e203a55f9"

inherit cmake pkgconfig multilib_header
SRC_URI = "git://github.com/libuvc/libuvc.git;protocol=https;tag=v${PV}"

S = "${WORKDIR}/git"

OECMAKE_FIND_ROOT_PATH_MODE_PROGRAM_class-target_qrb5165-rb5 = "BOTH"

DEPENDS = "libusb1 ccache-native"

do_configure_prepend() {
    mkdir -p ${S}/build
}

FILES_${PN} = "${libdir}/${PN}.so ${libdir}/aarch64-linux-gnu/${PN}.so ${libdir}/pkgconfig/libuvc.pc ${libdir}/../include/libuvc/libuvc.h ${libdir}/../include/libuvc/libuvc_config.h"

FILES_${PN}-dev = "${baselibdir}/aarch64-linux-gnu/${PN}.so ${baselibdir}/pkgconfig/libuvc.pc ${includedir}/libuvc/libuvc.h ${includedir}/libuvc/libuvc_config.h"

do_install() {
    install -d ${D}${libdir}
    install -d ${D}${includedir}/libuvc
    install -d ${D}${libdir}/aarch64-linux-gnu/
    install -d ${D}${libdir}/pkgconfig/
    install -m 644 ${S}/../build/libuvc.pc ${D}${libdir}/pkgconfig/libuvc.pc
    oe_libinstall -so libuvc ${D}${libdir}/
    cp ${D}${libdir}/libuvc.so ${D}${libdir}/aarch64-linux-gnu/
}

do_install_append() {
    install -d ${D}${includedir}/libuvc
    install -m 644 ${S}/include/libuvc/*.h ${D}${includedir}/libuvc/
    install -m 644 include/libuvc/libuvc_config.h -D ${D}${includedir}/libuvc/libuvc_config.h
}
